<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Class_computer */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Class Computer',
]) . $model->class_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Class Computers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->class_id, 'url' => ['view', 'id' => $model->class_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="class-computer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
