<?php

use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;


/* @var $this yii\web\View */
/* @var $model common\models\Studentupload */

$this->title = 'Upload Excel File';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Students and employee')];

?>
<div class="student-index wrapper col-md-offset-3 col-md-6">
<h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'file')->widget(FileInput::className(),[
    'name' => 'attachments', 
    'options' => [ 
        'id'=> 'input-701',
        'class'=>'file-loading',
        'data-allowed-file-extensions'=>'["xlsx"]',
        'multiple' => false, 
    ],
    
    'pluginOptions' => ['previewFileType' => 'any']
]) ?>

<?= Html::submitButton(Yii::t('app', 'submit'), ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>
</div>

       


   

    

