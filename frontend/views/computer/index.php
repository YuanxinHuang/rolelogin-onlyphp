<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ComputerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Computers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="computer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'serial_id',
            'computer_status',
//            'type_id',
            'model',
            'computer_name',
            // 'cpu',
            // 'ram',
            // 'storage_capacity',
            // 'shelf_placement',
            // 'purchase_date',
            // 'warranty_date',
            // 'end_dato',

               ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{view}',
                            'buttons'=>[
                              'view' => function ($url, $model) {     
                                return Html::a('<span class=" btn-rounded btn-info glyphicon glyphicon-list"></span>', $url, [
                                        'title' => Yii::t('yii', 'view'),
                                ]);                                
                              }
                          ]              
                ],
                       
        ],
    ]); ?>

</div>
