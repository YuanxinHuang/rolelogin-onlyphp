<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ComputerDamage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Computer Damages list'), 'url' => ['index']];
?>
<div class="computer-damage-view">

    <p>
        <?= Html::a(Yii::t('app', 'Back to list'), ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'serial_id',
            'damage_user_id',
            'damage_explain:ntext',
              [
                'attribute'=>'image',
                'value'=>$model->imageurl,
                'format' => ['image',['width'=>'400','alt'=>"damage computer:.$model->serial_id."]],
              ],
            'registered_at',
            'repaired_date',
        ],
    ]) ?>

</div>
