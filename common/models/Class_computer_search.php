<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Class_computer;

/**
 * Class_computer_search represents the model behind the search form about `common\models\Class_computer`.
 */
class Class_computer_search extends Class_computer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_id', 'class_describe'], 'safe'],
            [['allow_computer_type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Class_computer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'allow_computer_type' => $this->allow_computer_type,
        ]);

        $query->andFilterWhere(['like', 'class_id', $this->class_id])
            ->andFilterWhere(['like', 'class_describe', $this->class_describe]);

        return $dataProvider;
    }
}
