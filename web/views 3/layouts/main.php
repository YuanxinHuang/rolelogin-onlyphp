    <?php

/* @var $this \yii\web\View */
/* @var $content string */

error_reporting(E_ALL);
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use kartik\icons\Icon;
Icon::map($this);
//use rmrevin\yii\fontawesome\AssetBundle::register($this);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <meta charset="<?= Yii::$app->charset ?>">
    <!--to ensure proper rendering and touch zooming, which is mobile first of bootstrap-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!--container div start-->
<div class="all-container icon">
    <?php
    NavBar::begin([
        'brandLabel' =>'',
        'brandUrl' => Yii::$app->homeUrl,
        'brandOptions' => ['class' => 'brandLogo'],//options of the brand
        'options' => [
            'class' => 'navbar-default navbar-fixed-top headerbar',
        ],
    ]);  ?>


    <?=
    \lajax\languagepicker\widgets\LanguagePicker::widget([
        'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_DROPDOWN,
        'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_LARGE,
        'parentTemplate' => 
        '<ul class="navbar-nav navbar-right nav" style = "width:30px;">'
        . '<div class="language-picker language dropdown-list{size}">'
        . '<div id ="activeItem">{activeItem}<ul id = "uactiveItem">'
        . '{items}'
        . '</ul></div></div></ul> ',
    // JavaScripts
    ]);
    ?>


    <?php
//    $menuItems = [
//        ['label' => Icon::show('no', [], Icon::FI).'Norsk',
//            'options' => ['class' => 'language','onclick'=>"myFunction()"],
//            'url' => '#'],    
//        ['label' => Icon::show('gb', [], Icon::FI).'English', 
//            'options' => ['class' => 'language'],
//            'url' => '#']
//    ];
    if (Yii::$app->user->isGuest) {
    } else {     
        $menuItems[] = [
        'label' => Icon::show('home', [], Icon::FA). Yii::t('app', 'Welcome, ') .Yii::$app->user->identity->username, 
        'url' => ['/site/index'],
        'options' => ['class' => 'language'],
       'linkOptions' => ['data-method' => 'post']];
        $menuItems[] = [
            'label' => Icon::show('sign-out', [], Icon::FA).Yii::t('app','Logout'), 
            'url' => ['/site/logout'],
            'options' => ['class' => 'language'],
            'linkOptions' => ['data-method' => 'post']];
         echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
        'encodeLabels' => false,
    ]);
    }
   ?>

<?php NavBar::end(); ?>
    
         <!--fluid-aside div start--> 
    <div class="row-fluid-aside">
          <!--sidebar-menu div start-->
        
    <?php
    if (!Yii::$app->user->isGuest){
      echo '<div id="sidebar-menu">
              <!-- sidebar ul start-->
              <ul class="nav-list nav" id="sidebar-menu-id">                
                  <li class="sub-menu">
                      <label class="tree-toggle nav-header"><i class="fa fa-history" aria-hidden="true"></i>&nbsp<span class = "desktop-only">'.Yii::t('app','Damage').'</span></label>
                        <ul class="nav nav-list tree">
                        <li class="sub-menu-li"><a href="'.Yii::$app->request->baseUrl.'/admin.php/computerdamage">' .Yii::t('app', 
'All damage').'</a></li> 
    <li><a href="'.Yii::$app->request->baseUrl.'/admin.php/computerdamage/create">' .Yii::t('app', 
'New damages').'</a></li> 
                        </ul>
                  </li> 
                  
                  <li class="sub-menu"><label class="tree-toggle nav-header"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp<span class = "desktop-only">'.Yii::t('app', 
'Laptop Loans').'</span></label>
                            <ul class="nav nav-list tree">
                                <li class="sub-menu-li"><a href="'.Yii::$app->request->baseUrl.'/admin.php/student/checkout">' .Yii::t('app', 
'Add Many Loan').'</a></li>
                                <li><a href="'.Yii::$app->request->baseUrl.'/admin.php/studentequipment/create">' .Yii::t('app', 
'Add one Loan').'</a></li>
                                <li class="sub-menu-li"><a href="'.Yii::$app->request->baseUrl.'/admin.php/studentequipment">' .Yii::t('app', 
'Loans Overview').'</a></li>  <li class="sub-menu-li"><a href="'.Yii::$app->request->baseUrl.'/admin.php/studentequipment">' .Yii::t('app', 
'Return').'</a></li>
                            </ul>
                  </li>
                  <li class="sub-menu">
                  <label class="tree-toggle nav-header"><i class="fa fa-laptop" aria-hidden="true"></i>&nbsp<span class = "desktop-only">' .Yii::t('app', 
'Laptop').'</span></label>
                            <ul class="nav nav-list tree">
 
                                <li><a href="'.Yii::$app->request->baseUrl.'/admin.php/computer/create">' .Yii::t('app', 
'Register New Laptop').'</a></li>
                                <li class="sub-menu-li"><a href="'.Yii::$app->request->baseUrl.'/admin.php/computer/index">' .Yii::t('app', 
'Laptop Overview').'</a></li>
   
                            </ul>
                  </li>
                  <li class="sub-menu">
                      <label class="tree-toggle nav-header"><i class="fa fa-users" aria-hidden="true"></i>&nbsp<span class = "desktop-only">'. Yii::t('app', 
'Student/Employee').'</span></label>
                            <ul class="nav nav-list tree">
                               <li class="sub-menu-li"><a href="'.Yii::$app->request->baseUrl.'/admin.php/student/index?StudentSearch%5Brole_id%5D=10">' .Yii::t('app', 'Student').'</a></li>
                               <li class="sub-menu-li"><a href="'.Yii::$app->request->baseUrl.'/admin.php/student/index?StudentSearch%5Brole_id%5D=20">' .Yii::t('app', 'Teacher').'</a></li>
                               <li><a href="'.Yii::$app->request->baseUrl.'/admin.php/student/index">' .Yii::t('app', 'Users Overview').'</a></li> 
                               <li><a href="'.Yii::$app->request->baseUrl.'/admin.php/student/create">' .Yii::t('app', 'Form registration').'</a></li>                                
                               <li class="sub-menu-li"><a href="'.Yii::$app->request->baseUrl.'/admin.php/student/addexcel">' .Yii::t('app', 'Excel registration with students').'</a></li>
                               <li class="sub-menu-li"><a href="'.Yii::$app->request->baseUrl.'/admin.php/student/uploadform">' .Yii::t('app', 'Excel registration').'</a></li>
                            </ul>
                  </li>
                   
                  <li class="sub-menu"><label class="tree-toggle nav-header"><i class="fa fa-unlock-alt" aria-hidden="true"></i>&nbsp<span class = "desktop-only">' .Yii::t('app', 'Admin´s Password').'</span></label>
                                    <ul class="nav nav-list tree">
                       <li class="sub-menu"><a href="'.Yii::$app->request->baseUrl.'/admin.php/admin_user/changepassword">' .Yii::t('app', 'Change My Password').'</a></li>                 
                                    </ul>
                   </li>
                        <li class="sub-menu"><label class="tree-toggle nav-header"><i class="fa fa-unlock-alt" aria-hidden="true"></i>&nbsp<span class = "desktop-only">' .Yii::t('app', 'Student´s Password').'</span>
    </label>
                              <ul class="nav nav-list tree">
                       <li class="sub-menu"><a href="'.Yii::$app->request->baseUrl.'/admin.php/admin_user/changepassword">' .Yii::t('app', 'R a student´s password').'</label></a>
                       </li>
                       <li class="sub-menu"><a href="'.Yii::$app->request->baseUrl.'/admin.php/admin_user/changepassword">' .Yii::t('app', 'Password Resetting').'</a>
                       </li> 
                              </ul>
                   </li>

                    <li><label class="tree-toggle nav-header"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp<span class = "desktop-only">' .Yii::t('app', 'New Users').'</span></label>
                            <ul class="nav nav-list tree">
                                <li><a href="'.Yii::$app->request->baseUrl.'/admin.php/student/create">' .Yii::t('app', 'Register').'</a></li>
                            </ul>
                    </li>
                    
 <li><label class="tree-toggle nav-header"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp<span class = "desktop-only">' .Yii::t('app', 'New PC/MAC Model').'</span></label>
                            <ul class="nav nav-list tree">
                                <li><a href="'.Yii::$app->request->baseUrl.'/admin.php/laptopmodel/create">' .Yii::t('app', 'Register').'</a></li>
                            </ul>
                    </li>
                    
 <li><label class="tree-toggle nav-header"><i class="fa fa-user-md" aria-hidden="true"></i>&nbsp<span class = "desktop-only">' .Yii::t('app', 'My info:Admins') .'</span></label>
                            <ul class="nav nav-list tree">
                                <li><a href="'.Yii::$app->request->baseUrl.'/admin.php/admin_user/mypage">' .Yii::t('app', 'My info') .'</a></li>
                            </ul>
                    </li>
                    
<li><label class="tree-toggle nav-header"><i class="fa fa-user-md" aria-hidden="true"></i>&nbsp<span class = "desktop-only">' .Yii::t('app', 'Overview of roles') .'</span></label>
                            <ul class="nav nav-list tree">
                                <li><a href="'.Yii::$app->request->baseUrl.'/admin.php/admin/role">' .Yii::t('app', 'Overview of roles') .'</a></li>
                            </ul>
                    </li>
                
                    <!-- sidebar ul end-->
              </ul> </div>
                  <!--sidebar-menu div end-->
        <!--sider aside end-->';
      
        }    
        ?>
           
       <div id = "main_content_admin">
        <div id = "main_container_admin">
      
              <?php echo
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]);
     
              ?>
         
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
     </div>
    <!-- fluid-aside div end-->
</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
