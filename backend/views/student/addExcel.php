<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel common\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

    $this->title = 'Users';
    $this->params['breadcrumbs'][] = $this->title;

?>

    <div class="student-index wrapper ">
    <div class="inline">
        <?php Pjax::begin(['timeout' => 3000]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'ajaxUpdate'=>true,
//            'template'=>'{pager}{summary}{items}{pager}',
            'filterModel' => $searchModel,
            'columns' => [
    //            ['class' => 'yii\grid\SerialColumn'],
                'id',
                'username',
    //            'auth_key',
    //            'password_hash',
    //            'password_reset_token',
                 'email:email',
                 'role_id',
                // 'user_type_id',
                 'status_id',
                 'created_at',
                 'ended_at',
                'class_nr', 
                 ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{view} {update}',
                            'buttons'=>[
                              'view' => function ($url, $model) {     
                                return Html::a('<span class=" btn-rounded btn-info glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('yii', 'view'),
                                ]);                            
                              },
                               'update' => function ($url, $model) {     
                                return Html::a('<span class=" btn-rounded btn-primary glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('yii', 'update'),
                                ]);                                
                              }
                          ]              
             ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
