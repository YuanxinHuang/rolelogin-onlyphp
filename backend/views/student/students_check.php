

<?php use yii\helpers\Html;
use yii\web\View;?>

    <?php
          $this->registerCssFile(Yii::$app->request->BaseUrl . '/myassets/vendor/DataTables/css/dataTables.bootstrap.css');
          $this->registerCssFile(Yii::$app->request->BaseUrl . '/myassets/vendor/fontawesome/css/font-awesome.min.css');
          $this->registerCssFile(Yii::$app->request->BaseUrl . '/myassets/vendor/themify-icons/themify-icons.min.css');

    ?>

    <h1><?=Yii::t('app', 'Please Choose Users') ?></h1>
    <h1><?= Yii::t('app', 'Step 1')?></h1>
    <p><?=Yii::t('app','To borrow laptop from the ICT department you need to send requests for borrowing.')?></p>
    <p><?=Yii::t('app','In this step, you should choose at least one person before you can go further.')?></p>
    <p><?=Yii::t('app','You can combine all users in this step, both students, teachers and accountants.')?></p>
<?=Html::beginForm( ['student/checkout'],'post');?>
<!-- start: ui bootstrap accordion -->
<div class="container-fluid container-fullw">
    <div class="row" style="margin-top: 50px;">
        <div class="col-lg-12">

            <div class="panel-group accordion" id="accordion">
                <div class="panel panel-white">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseone"
                       style="text-decoration: none;">
                        <div class="panel-heading" style="background-color: #34363D;">
                            <h5 class="panel-title">
                                <i class="fa fa-angle-down" style="float: right"></i><?php echo count($data["teacher"]); ?><span class="glyphicon glyphicon-user"></span>Student
                            </h5>
                        </div>
                    </a>
                    <div id="collapseone" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                <thead>
                                <tr>
                                    <th>Username</th>
                                    <th> Email</th>
                                    <th class="hidden-xs"> Start At</th>
                                    <th class="hidden-xs"> End At</th>
                                    <th>Class_nr</th>
                                    <th>Loan <input class="student_check" type="checkbox" id="student_total" ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach( $data["student"] as $row )
                                    {?>
                                    <tr>
                                        <td><?=$row['username']?></td>
                                        <td><?=$row['email']?></td>
                                        <td><?=$row['created_at']?></td>
                                        <td><?=$row['ended_at']?></td>
                                        <td><?=$row['class_nr']?></td>
                                        <td><input class="student_check" type="checkbox" name="keylist[]" value="<?=$row['id'] ?>"></td>
                                    </tr>

                                    <?php
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-white">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo"
                       style="text-decoration: none;">
                        <div class="panel-heading" style="background-color: #34363D;">
                            <h5 class="panel-title">
                                <i class="fa fa-angle-down" style="float: right"></i><?php echo count($data["teacher"]); ?><span class="glyphicon glyphicon-user"></span>Teacher
                            </h5>
                        </div>
                    </a>
                    <div id="collapsetwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                <thead>
                                <tr>
                                    <th>Username</th>
                                    <th> Email</th>
                                    <th class="hidden-xs"> Start At</th>
                                    <th class="hidden-xs"> End At</th>
                                    <th>Class_nr</th>
                                    <th>Loan  <input class="teacher_check" type="checkbox" id="teacher_total" ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach( $data["teacher"] as $row )
                                {?>
                                    <tr>
                                        <td><?=$row['username']?></td>
                                        <td><?=$row['email']?></td>
                                        <td><?=$row['created_at']?></td>
                                        <td><?=$row['ended_at']?></td>
                                        <td><?=$row['class_nr']?></td>
                                        <td><input class="teacher_check" type="checkbox" name="keylist[]" value="<?=$row['id'] ?>"></td>
                                    </tr>

                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-white" >
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsethree"
                       style="text-decoration: none;">
                        <div class="panel-heading" style="background-color: #34363D;">
                            <h5 class="panel-title">
                                <i class="fa fa-angle-down" style="float: right"></i>Accounting Department
                            </h5>
                        </div>
                    </a>
                    <div id="collapsethree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover table-full-width sticky-header" id="sample_3">
                                <thead>
                                <tr>
                                    <th>Username</th>
                                    <th> Email</th>
                                    <th class="hidden-xs"> Start At</th>
                                    <th class="hidden-xs"> End At</th>
                                    <th>Class_nr</th>
                                    <th>Loan  <input class="department_check" type="checkbox" id="department_total" ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach( $data["accounting_department"] as $row )
                                {?>
                                    <tr>
                                        <td><?=$row['username']?></td>
                                        <td><?=$row['email']?></td>
                                        <td><?=$row['created_at']?></td>
                                        <td><?=$row['ended_at']?></td>
                                        <td><?=$row['class_nr']?></td>
                                        <td><input class="department_check" type="checkbox" name="keylist[]" value="<?=$row['id'] ?>"></td>
                                    </tr>

                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=Html::submitButton(Yii::t('app', 'Send requests to Computer Department'), [
    'class' => 'btn btn-primary',
]);?>
<?= Html::endForm();?>
<!-- end: ui bootstrap accordion -->
<?php
    $this->registerJsFile(Yii::$app->request->BaseUrl . '/myassets/vendor/select2/select2.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::$app->request->BaseUrl . '/myassets/vendor/DataTables/jquery.dataTables.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::$app->request->BaseUrl . '/myassets/common.js', ['depends' => [yii\web\JqueryAsset::className()]]);

?>
<script type="text/javascript">
    var flag = '<?php
     if(isset($flag))
            echo $flag?>';
    if(flag == '1')
        alert("Please select at least one user.");
</script>
