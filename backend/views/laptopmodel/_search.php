<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Laptopmodel_search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="laptopmodel-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'model_id') ?>

    <?= $form->field($model, 'model_name') ?>

    <?= $form->field($model, 'cpu') ?>

    <?= $form->field($model, 'ram') ?>

    <?= $form->field($model, 'storage_capacity') ?>

    <?php // echo $form->field($model, 'computer_type_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
