<?php
/**
 * Created by NetBeans.
 * User: Yuanxin Huang
 * Date: 5/1/2016
 * Time: 10:29 PM
 */

namespace common\models;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use Yii;

class StudentLoan extends ActiveRecord
{
    public $start_at;
    public $end_at;
    public function rules()
    {
        return [
            ['start_at', 'required'],
            ['end_at', 'required'],
        ];
    }

    public static function tableName()
    {
        return "user_equipment";
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'start_at' => 'Start Date',
            'end_at' => 'End Date',
        ];
    }

    public function loanEquipment($ids)
    {
        foreach($ids as $user_id)
        {

            $sql = "INSERT INTO {$this->tableName()} (renter_id,serial_id,borrow_status_id,require_by_teacher_id,start_at,end_at)
                    VALUES (:renter_id,:serial_id,:borrow_status_id,:require_by_teacher_id,:start_at,:end_at)";
            $command=$this->getDB()->createCommand($sql);
            $command->bindValue(':renter_id',$user_id);
            $command->bindValue(':serial_id',NULL);
            $command->bindValue(':borrow_status_id',0);
            $command->bindValue(':require_by_teacher_id',10);
            $command->bindValue(':start_at',$this->start_at);
            $command->bindValue(':end_at',$this->end_at);
            $command->execute();

        }
    }
}