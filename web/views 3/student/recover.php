<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Student */

$this->title = 'Update '.$model->role->role_name.' User: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-update">

    <h1><?= Html::encode($this->title) ?></h1>

  <div class="student-form">

    <?php error_reporting(E_ALL); 
    $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['deviceSize' => ActiveForm::SIZE_SMALL],
]); ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
    'data' => $model->getStatusList(),
    'options' => ['placeholder' => 'Select a status name ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ])->label('Status') ?>
    
<div>
    <hr>
    <?= Html::submitButton('submit information', ['class' => 'btn btn-success']) ?>
    <?= Html::a('Add new class', 
            Yii::$app->request->baseUrl.'/admin.php/site/logout',['class' => 'btn btn-success']) ?>
</div>
    <?php ActiveForm::end(); ?>

</div>
</div>
