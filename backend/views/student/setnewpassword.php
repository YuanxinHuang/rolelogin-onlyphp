<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Student */
$this->title = 'Update password';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Users list'), 'url' => ['passwordlist']];
$this->params['breadcrumbs'][] = 'Update password';
?>
<div class="student-update">

    <h1><?= Html::encode($this->title) ?></h1>

  <div class="student-form col-md-offset-3 col-md-6">
   <?php $form = ActiveForm::begin(); ?>
 
   <?= $form->field($model, 'password_new')->passwordInput() ?> 

   <?= $form->field($model, 'password_new')->passwordInput() ?> 
   <div class="form-group">
      <?= Html::submitButton('submit', ['class' => 'btn btn-success']) ?>
   </div>
   <?php ActiveForm::end(); ?>

</div>

</div>
