<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\Student */

$this->title = 'Update System user: ' . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-update wrapper">

    <h1><?= Html::encode($this->title) ?></h1>
    <span>User ID:</span><p><?= $model->id?></p>

  <div class="student-form">
    <?php $form = ActiveForm::begin(); ?>
 
    <?= $form->field($model, 'status_id')->widget(Select2::classname(), [
        'data' => $model->getStatusList(),
        'language' => 'no',
        'options' => ['placeholder' => 'Select a status name..'],
        'pluginOptions' => [
            'allowClear' => true
              ],
        ]) ?>
   <div class="form-group">
      <?= Html::submitButton('submit', ['class' => 'btn btn-success']) ?>
   </div>
    <?php ActiveForm::end(); ?>

</div>

</div>
