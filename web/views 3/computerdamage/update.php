<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ComputerDamage */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Computer Damage',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Computer Damages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="computer-damage-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
