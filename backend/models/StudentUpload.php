<?php

namespace backend\models;

use common\models\Student;
use yii\base\Model;
use backend\models\StudentCreate;
use Yii;
use PHPExcel_CachedObjectStorageFactory;

//use common\models\Student;
/**
 * Signup form
 * Form model to enforce validation rules or other methods
 * data comes in from a controller
 */
class StudentUpload {

    public $username;
    public $email;
    public $password;
    public $role_id;
    public $created_at;
    public $ended_at;
    public $class_nr;



    public function getUploadFiledata() {
        $inputFil = '@webroot/uploads/test.xlsx';
//        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite;
//if (PHPExcel_Settings::setCacheStorageMethod($cacheMethod)) {
//    echo date('H:i:s') , " Enable Cell Caching using " , $cacheMethod , " method" , EOL;
//} else {
//    echo date('H:i:s') , " Unable to set Cell Caching using " , $cacheMethod , " method, reverting to memory" , EOL;
//}
        try {
            $objPHPExcel = \PHPExcel_IOFactory::load(\Yii::getAlias
                                    ($inputFil));
        } catch (\PHPExcel_Reader_Exception $e) {
            die('Error loading file'.$e->getMessage());
        }
        $sheetData = $objPHPExcel->getActiveSheet();
        return $sheetData;
        //array data
    }

    public function displayData() {
        $sheetData = $this->getUploadFiledata();
        // Get the highest row number and column letter referenced in the worksheet
        $highestRow = $sheetData->getHighestRow(); // e.g. 12
        $highestColumn = $sheetData->getHighestDataColumn(); // e.g 'I'
        $highestColumn++;

        echo '<h2>Data:</h2><div class="table-responsive"><table class="table"><tr>';
        echo '<td>User nummber</td><td>Email</td><td>CREATE AT</td><td>END AT</td></tr>';
        for ($row = 1; $row <= $highestRow; ++$row) {
                echo "<tr>";
            for ($col = 'A'; $col <= $highestColumn; ++$col) {  
                switch ($col) {
                    case 'A':
                        $this->username = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'B':
                         $this->password = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'C':
                         $this->email = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'D':
                        $this->role_id = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'E':
                        $this->created_at = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'F':
                        $this->ended_at = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'G':
                        $this->class_nr = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    default:
                        break;
                }
            }
            echo "<td>" . $this->username."</td><td>".$this->email."</td><td>".$this->created_at."</td><td>".$this->ended_at."</td>";
            echo "</tr>";    
        }
        echo "</table></div>";
    }

    public function insertData() {
        $sheetData = $this->getUploadFiledata();
        // Get the highest row number and column letter referenced in the worksheet
        $highestRow = $sheetData->getHighestRow(); // e.g. 12
        $highestColumn = $sheetData->getHighestDataColumn(); // e.g 'I'
        $bool = [];
        for ($row = 1; $row <= $highestRow; $row++) {
            for ($col = 'A'; $col <= $highestColumn;$col++) {    
                switch ($col) {
                    case 'A':
                        $this->username = (string)$sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'B':
                         $this->password = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'C':
                         $this->email = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'D':
                        $this->role_id = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'E':
                        $this->created_at = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'F':
                        $this->ended_at = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    case 'G':
                        $this->class_nr = $sheetData->getCell($col . $row)
                                            ->getValue();
                        break;
                    default:
                        break;
                }
            }
            if (!$this->signup()) {
                $bool[] = 'ERROR';
            } 
        }
         return count($bool)==0;
    }
    
      public function signup()
    {
            $user = new Student();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->username);
            $user->user_type_id = 10;
            $user->role_id = $this->role_id;
            $user->created_at = $this->created_at;
            $user->ended_at = $this->ended_at;
            $user->class_nr = $this->class_nr;
            if($user->save())
            {
            $auth = Yii::$app->authManager;
            $authorRole = $auth->getRole($user->getRoleName());
            $auth->assign($authorRole, $user->getId());
            return $user;
            } else return false;
    }
    
}
