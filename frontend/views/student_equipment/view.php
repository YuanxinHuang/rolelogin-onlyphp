<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StudentEquipment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-equipment-view wrapper">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'renter_id',
            'serial_id',
            'borrow_status_id',
            'require_by_teacher_id',
            'start_at',
            'end_at',
        ],
    ]) ?>

</div>
