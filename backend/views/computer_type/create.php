<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ComputerType */

$this->title = Yii::t('app', 'Create Computer Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Computer Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="computer-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
