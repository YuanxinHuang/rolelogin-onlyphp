<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    
    'modules' => [
        'gridview' =>  [
        'class' => '\kartik\grid\Module',
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
        ]
    ],
    'language' => 'nb-NO',
    'bootstrap' => ['languagepicker'],
    'components' => [
        'pdf' => [
        'class' => kartik\mpdf\Pdf::classname(),
        'format' => kartik\mpdf\Pdf::FORMAT_A4,
        'orientation' => kartik\mpdf\Pdf::ORIENT_PORTRAIT,
        'destination' => kartik\mpdf\Pdf::DEST_BROWSER,
        // refer settings section for all configuration options
    ],
        'languagepicker' => [
            'class' => 'lajax\languagepicker\Component',
            'languages' => ['en' => 'EN', 'nb-NO' => 'NO'],     // List of available languages (icons only)
            'cookieName' => 'language',                         // Name of the cookie.
            'expireDays' => 2,                                 // The expiration time of the cookie is 64 days.
            'callback' => function() {
    //            if (!\Yii::$app->user->isGuest) {
    //                $user = \Yii::$app->user->identity;
    //                $user->language = \Yii::$app->language;
    //                $user->save();
    //            }
            }
    ],
       'view' => [
            'theme' => [
                'pathMap' => [                
                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'             
                 ],
            ],
    ],
        'user' => [
            'identityClass' => 'common\models\AdminUser',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            //for security reasons, better enableStrictParsing to see what is exposed to the public.
        ],
        
          'authManager' => [
            'class' => 'yii\rbac\DbManager',
//            'itemTable' => 'auth_item',
//            'assignmentTable' => 'auth_assignment',
//            'itemChildTable' => 'auth_item_child',
        ],
        'i18n' => [
        'translations' => [
            'app*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@yii/messages',
                //'sourceLanguage' => 'en-US',
                'fileMap' => [
                    'app' => 'app.php',
                    'app/error' => 'error.php',
                ],
                'forceTranslation' => true
            ],
        ],
    ],
    ],
    
        'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
//            'admin/*',
        ]
    ],
    'params' => $params,
];
