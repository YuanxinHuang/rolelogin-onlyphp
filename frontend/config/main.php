<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    
    'modules' => [
        'gridview' =>  [
        'class' => '\kartik\grid\Module',
        ],
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        
        'user' => [
            'identityClass' => 'common\models\Student',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
          'urlManager' => [
            'enablePrettyUrl' => true,
            //for security reasons, better enableStrictParsing to see what is exposed to the public.
        ],
        
            'authManager' => [
            'class' => 'yii\rbac\DbManager',
//            'itemTable' => 'auth_item',
//            'assignmentTable' => 'auth_assignment',
//            'itemChildTable' => 'auth_item_child',
        ],
    ],
      'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            'admin/*',
        ]
    ],
    'params' => $params,
];
