<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Computer Damages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="computer-damage wrapper">
    <h1><i class="fa fa-history"></i><?= Html::encode($this->title) ?></h1>
    <?php 
//    echo $this->render('_search', ['model' => $searchModel]); 
    ?>
<div class="row">
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
             [
                 'attribute'=>'serial_id',
                 'label' => Yii::t('app', 'Serial number'),
                 'value'=>'serial_id',
             ],
            'damage_user_id',
               [
                 'attribute'=>'damage_explain',
                 'label' => Yii::t('app', 'Damage Explain'),
                 'value'=>'damage_explain',

             ],
              [
                 'attribute'=>'registered_at',
                 'value'=>'registered_at',
                    'label' => Yii::t('app', 'Registered Date'),
             ],
              [
                 'attribute'=>'repaired_date',
                 'value'=>'repaired_date',
             ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'options' => ['class' => 'col-md-12'],
        'export' => false,
    ]); ?>
<?php Pjax::end(); ?>
</div>
</div>
