<?php

namespace frontend\controllers;

use Yii;
use common\models\Student_equipment;
use common\models\Student_equipment_create;
use common\models\StudentEquipmentSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * StudentEquipmentController implements the CRUD actions for StudentEquipment model.
 */
class Student_equipmentController extends Controller
{
    public function behaviors()
    {
        return [
             'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['view'
                            ,'create','delete','teachers_request','my_rent','teacher_request_rent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentEquipment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Student_equipment::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StudentEquipment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
         public function actionMy_rent() {
          $searchModel = new StudentEquipmentSearch(); 
          $dataProvider = $searchModel->search([
            'StudentEquipmentSearch' => 
            ['renter_id' => Yii::$app->user->id]]);
         return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
    }
    
    
    /**
     * Displays a single StudentEquipment model.
     * @param integer $id
     * @return mixed
     */
    public function actionTeachers_request()
    {
            $searchModel = new StudentEquipmentSearch(); 
//          $mySearch = $searchModel->search($params)
//          $searchModel->require_by_teacher_id
            $dataProvider = $searchModel->search([
                'StudentEquipmentSearch' => 
                ['require_by_teacher_id' => Yii::$app->user->id]]);
           return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionTeacher_request_rent(){
        $model = new Student_equipment_create();

        if ($model->load(Yii::$app->request->post()) && $model->teacher_request_rent()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('add_peroid', [
//                'model' => $model,
                'selection'=> $selection,
            ]);
        }
        
    }

    /**
     * Creates a new StudentEquipment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Student_equipment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StudentEquipment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Deletes an existing StudentEquipment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StudentEquipment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentEquipment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Student_equipment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
