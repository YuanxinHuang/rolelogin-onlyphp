<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Computer Damages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="computer-damage-index wrapper">

    <h1><i class="fa fa-history"></i><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Computer Damage'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'serial_id',
            'damage_user_id',
            'damage_explain:ntext',
//             [
//                'attribute'=>'image',
//                'value'=>$model->imageurl,
//                'format' => ['image',['width'=>'100']],
//              ],
            'registered_at',
            'repaired_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'export' => false,
    ]); ?>
<?php Pjax::end(); ?>
</div>
