<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "computer_damage".
 *
 * @property integer $id
 * @property string $serial_id
 * @property integer $damage_user_id
 * @property string $damage_explain
 * @property string $image
 * @property string $registered_at
 * @property string $repaired_date
 *
 * @property Computer $serial
 * @property Student $damageUser
 */
class ComputerDamage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'computer_damage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serial_id', 'damage_user_id', 'registered_at'], 'required'],
            [['damage_user_id'], 'integer'],
            [['damage_explain'], 'string'],
            [['registered_at', 'repaired_date'], 'safe'],
            [['serial_id'], 'string', 'max' => 25],
            [['image'], 'safe'],
            [['serial_id'], 'unique'],
            [['damage_user_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'serial_id' => 'Serial ID',
            'damage_user_id' => 'Damage User ID',
            'damage_explain' => 'Damage Explain',
            'image' => 'Image',
            'registered_at' => 'Registered At',
            'repaired_date' => 'Repaired Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSerial()
    {
        return $this->hasOne(Computer::className(), ['serial_id' => 'serial_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDamageUser()
    {
        return $this->hasOne(Student::className(), ['id' => 'damage_user_id']);
    }
    
     public function getStudentLink()
    {
    $url = Url::to(['student/view', 'id'=>$this->damage_user_id]);
    $options = [];
    return Html::a($this->damageUser ? 'damageUser' : 'none', $url, $options);
    }
    
     public function getImageurl()
    {
    return Yii::$app->request->BaseUrl.'/uploads/'.$this->image;
    }
}
