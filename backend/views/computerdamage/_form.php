<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
use yii\helpers\ArrayHelper;
use common\models\Computer;
use common\models\Student;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\ComputerDamage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="computer-damage-form">

    <?php error_reporting(E_ALL); 
    $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['deviceSize' => ActiveForm::SIZE_SMALL],
]); ?>

    <?= $form->field($model, 'serial_id')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(Computer::find()->all(),'serial_id','computer_name'),
    'options' => ['placeholder' => 'Select a Computer name..'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ]) ?>

    <?= $form->field($model, 'damage_user_id')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(Student::find()->all(),'id','username'),
    'language' => 'no',
    'options' => ['placeholder' => 'Select a student name..'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ]) ?>

    <?= $form->field($model, 'damage_explain')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image')->fileInput() ?>
    
    <?= $form->field($model, 'registered_at')->widget(DatePicker::className(),[
    'name' => 'registered_at', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>

    <?= $form->field($model, 'repaired_date')->widget(DatePicker::className(),[
    'name' => 'repaired_date', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php ActiveForm::end(); ?>

</div>
