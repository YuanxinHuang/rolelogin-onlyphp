<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Student_equipment;

/**
 * StudentEquipmentSearch represents the model behind the search form about `common\models\Student_equipment`.
 */
class StudentEquipmentSearch extends Student_equipment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'renter_id', 'require_by_teacher_id', 'start_at', 'end_at'], 'integer'],
            [['serial_id', 'borrow_status_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Student_equipment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        
        $query->joinWith('status');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
//            return $dataProvider;
        }
        
        $query->joinWith('serial');

        $query->andFilterWhere([
            'renter_id' => $this->renter_id,
            'require_by_teacher_id' => $this->require_by_teacher_id,
            'start_at' => $this->start_at,
            'end_at' => $this->end_at,
        ]);

        $query->andFilterWhere(['like', 'computer.computer_name', $this->serial_id])
           ->andFilterWhere(['like', 'borrow_status.borrow_status_name', $this->borrow_status_id]);

        return $dataProvider;
    }
}
