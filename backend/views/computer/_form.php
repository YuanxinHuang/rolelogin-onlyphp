<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\ComputerType;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
/* @var $this yii\web\View */
/* @var $model common\models\Computer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="computer-form">

    <?php error_reporting(E_ALL); 
    $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['deviceSize' => ActiveForm::SIZE_SMALL],
]); ?>

    <?= $form->field($model, 'serial_id', [
    'hintType' => ActiveField::HINT_SPECIAL,
    'hintSettings' => ['placement' => 'right', 'onLabelClick' => true, 'onLabelHover' => false]
])->hint('serial_id needs to be unique.');
?>
      <?= $form->field($model, 'model')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(common\models\Laptopmodel::find()->all(),'model_name','model_name'),
    'language' => 'no',
    'options' => ['placeholder' => 'Select a model type ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ]) ?>

    <?= $form->field($model, 'computer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shelf_placement')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(common\models\Computer::find()->all(),'shelf_placement','shelf_placement'),
    'size' => Select2::MEDIUM,
    'language' => 'no',
    'options' => ['placeholder' => 'Select a shelf placement ...'],
    'pluginOptions' => [
        'hideSearch'=> true
          ],
    ]) ?>
    <?= $form->field($model, 'purchase_date')->widget(DatePicker::className(),[
    'name' => 'purchase_date', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>

    <?= $form->field($model, 'warranty_date')->widget(DatePicker::className(),[
    'name' => 'warranty_date', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>
    
    <?= $form->field($model, 'end_dato')->widget(DatePicker::className(),[
    'name' => 'end_dato', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>
        <!-- Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])--> 
    <?= Html::submitButton(Yii::t('app', 'submit'), ['class' => 'btn btn-success']) ?>

    <?php ActiveForm::end(); ?>

</div>