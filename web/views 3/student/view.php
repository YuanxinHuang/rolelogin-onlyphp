<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $model common\models\Student */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view wrapper">

    <h1><?= Html::encode($this->title) ?></h1>
<?php
Modal::begin([
    'header' => '<h4 class="modal-title">Detail information</h4>',
    'toggleButton' => ['label' => '<i class="glyphicon glyphicon-th-list"></i> Detail View in Modal', 'class' => 'btn btn-primary']
]);
echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
              [
                'attribute'=>'username',
                'value'=>$model->username,
                'options' => [
                    'class' => 'col-sm-12',
                ],
              ],
            'className', 
            'email:email',
            'role.role_name',         
            'allowedComputerName', 
            [
                'label' => 'This person status:',
                'attribute'=>'status_id',
                'value'=>$model->status->status_name,
             ],
              [
                'label' => 'This person started school at:',
                'attribute'=>'created_at:datetime',
                'value'=>$model->created_at,
             ],
            [
                'label' => 'This person ended school at:',
                'attribute'=>'ended_at:datetime',
                'value'=>$model->ended_at,

             ],
            'class_nr' 
        ],
    ]); 
Modal::end();?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

 

</div>
