<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View        //<?= Html::a('Create Student Equipment', ['href = "'.Yii::$app->request->baseUrl.'/index.php/student/checkout"'], ['class' => 'btn btn-success']) ?> */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Student Equipments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-equipment-index wrapper">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if (Yii::$app->user->can('send_student_list')){
        echo '<a href="'.Yii::$app->request->baseUrl.'/index.php/student/checkout" class = "btn btn-success">legg til ny utlån</a>';
        }?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'renter_id',
             [ 
            'label' => Yii::t('app','Laptop'),
            'attribute'=>'serial_id',
            'value'=>'serial.computer_name',
            ],
             [    
                 'label' => Yii::t('app','Lap status'),
                 'attribute'=>'computer_status',
                 'value'=>'serial.computer_status',
            ],
             [
                'label' => Yii::t('app','Application status'),
                'attribute' => 'borrow_status_id',
                'value' => 'status.borrow_status_name',
            ],
             [
                'label' => Yii::t('app','Application sendt by'),
                'attribute' => 'require_by_teacher_id',
                 'value' => 'teacher.username',
            ],
            'start_at',
            'end_at',
                ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{view}',
                            'buttons'=>[
                              'view' => function ($url, $model) {     
                                return Html::a('Detail<span class="glyphicon glyphicon-plus"></span>', $url, [
                                        'title' => Yii::t('yii', 'View'),
                                ]);                                
                              }
                          ]                            
                ],
        ],
    ]); ?>

</div>
