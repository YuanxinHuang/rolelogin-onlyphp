<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
?>
 <h1><?php echo Yii::t('app', 'Check this list before send request to ICT Department'); ?></h1>
 <h1><?php echo Yii::t('app', 'Step 2'); ?></h1>
<?php $form = ActiveForm::begin(); ?>
   

<div class="panel panel-white wrapper">
   
    <p><?php echo Yii::t('app', 'You can click the back button if you want to change your choice'); ?></p>
<div class="panel-body">
    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1" style="margin-top: 50px">
        <thead>
        <tr>
            <th>Username</th>
            <th>Email</th>
            <th class="hidden-xs"> Start At</th>
            <th class="hidden-xs"> End At</th>
            <th>Class_nr</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach( $data as $row )
        {?>
            <tr>
                <td><?=$row['username']?></td>
                <td><?=$row['email']?></td>
                <td><?=$row['created_at']?></td>
                <td><?=$row['ended_at']?></td>
                <td><?=$row['class_nr']?></td>
                <td class="hidden"> <input type="hidden" name="idlist[]" value="<?=$row['id']?>"> </td>
            </tr>
        <?php
        }
        ?>

        </tbody>
    </table>
</div>
</div>
<?= $form->field($model, 'start_at')->widget(DatePicker::className(),[
    'name' => 'start_at',
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>

<?= $form->field($model, 'end_at')->widget(DatePicker::className(),[
    'name' => 'end_at',
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]])?>

<?= Html::submitButton(Yii::t('app','Submit '), [
    'class' => 'btn btn-success',
    'data-toggle'=>'tooltip', 
    'title'=>'Send requests to ICT department, You cannot change after sending.'
    ]) ?>
<?= Html::resetButton(Yii::t('app','Reset Date'), [
    'class' => 'btn btn-default',
    'data-toggle'=>'tooltip', 
    'title'=>'Will reset dates that you have chosen.'
    ]) ?>
<?= Html::a(Yii::t('app','Back to list'), ['checkout'], [
    'class' => 'btn btn-info',
    'target'=>'_blank', 
    'data-toggle'=>'tooltip', 
    'title'=>'Will come back to the list page.'
    ]) ?>
<?php ActiveForm::end(); ?>

 <?php
    $this->registerCssFile(Yii::$app->request->BaseUrl . '/myassets/vendor/DataTables/css/dataTables.bootstrap.css');
 ?>

