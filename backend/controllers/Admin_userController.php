<?php

namespace backend\controllers;

use Yii;
use common\models\AdminUser;
use backend\models\ChangePasswordForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\models\UploadForm;
use yii\web\UploadedFile;
//use phpexcel\Classes\PHPExcel;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use kartik\widgets\Alert;

class Admin_userController extends Controller
{
    public function behaviors()
    {
        return [
      
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
                    'access' => [
                        'class' => AccessControl::className(),
                        'rules' => [
                               // allow authenticated users
                            [
                                'actions' => ['changepassword','resetpassword','view','mypage'],
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                        ],
            ],
        ];
    }

    /**
     * Displays a single Admin model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Admin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    /**
     * Creates a new view profile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
 
     public function actionMypage() {
        $id =  Yii::$app->user->id;
        $model = $this->findModel($id);
        return $this->render('view', ['model' => $model]);
    }
    
    
      /**
     * Updates an existing Admin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionUploadform()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->upload()) {
                    // file is uploaded successfully
                 Yii::$app->session->setFlash('success', 'You had added a file.');
}           return $this->redirect(['upload']);
            }
    return $this->render('upload', ['model' => $model]);
    }
    
       /**
     * Updates an existing Admin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

     public function actionUpload()
    {
        $model = new StudentUpload();

        if (Yii::$app->request->isPost) {
                    if ($model->insertData()) {
                    // data is uploaded successfully to db
                    
                    Yii::$app->session->setFlash('success', ' Greate! You had added USERS.');
                    return $this->redirect(['index']);
                    
                    } else {
            
                    Yii::$app->session->setFlash('success', ' Sorry! You had not added any USERS.');
                    }
     }
        return $this->render('readUpload', [
                'model' => $model,
    ]);
    }
    
     /**
     * checkoutbox an existing Admin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionCheckout()
    {
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (isset($_POST['keylist'])) {
        $keys = \yii\helpers\Json::decode($_POST['keylist']);
        if (!is_array($keys)) {
            echo Json::encode([
                'status' => 'error'
            ]);
            return;
        }
        // you could alternatively write a single query using 
        // SQL IN CONDITION, instead of loop below to fetch the total 
        // from DB in ONE SINGLE fetch from the DB.
        
        foreach ($keys as $key) {
           $newstudent = findModel($key);
           // get total price from your model's column e.g. price
           Yii::$app->session->setFlash('success',  '$newstudent');
                    }
        } else {
            return $this->render('checkbox', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing Admin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'You have change your infor');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('success',  'You havn´t done any changes yet');
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        } else { 
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
     public function actionResetpassword(){
        $id=Yii::$app->user->id;
        $_user = $this->findModel($id);
        $model= new ChangePasswordForm();
            if ($model->load(Yii::$app->request->post())) {        
                try {
                $model->setnewPassword($_user);
               $success = Yii::$app->session->setFlash('notice',  'Congratulations! You had changed your password successfully!');
                echo Alert::widget([
                'type' => Alert::TYPE_DANGER,
                'title' => 'Error Logo Exists!',
                'icon' => 'glyphicon glyphicon-exclamation-sign',
                'body' => $success,
                'showSeparator' => true,
                'delay' => $base_delay,
            ]);
                return $this->redirect(['mypage']);
                }catch(db\IntegrityException $e)
                    {
                        $e;
                        Yii::$app->session->setFlash('notice',$e);
                    }
            } else {
            return $this->render('changepassword', [
                'model' => $model,
                ]);
            }
    }

    /**
     * Deletes an existing Admin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Admin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Admin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdminUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Send debug code to the Javascript console
     * @param integer $data
    */ 
    public function actionChangepassword(){
        $id=Yii::$app->user->id;
        $_user = $this->findModel($id);
        $model= new ChangePasswordForm();
            if ($model->load(Yii::$app->request->post())) {
                if($_user->validatePassword($model->password_old)){
                    if($model->setnewPassword($_user) ){
                    Yii::$app->session->setFlash('success',  'You had changed your password');
                    return $this->redirect(['mypage']);
                    } else {
                    Yii::$app->session->setFlash('error',  'You had not changed your password');
                    return $this->redirect(['mypage']);}
                } else {
                    Yii::$app->session->setFlash('error', 'Your old password is not correct, please enter again');
                    return $this->redirect(['changepassword']);
                }
            } else {
            return $this->render('changepassword', ['model' => $model,
                ]);
        }
    }
    

}
