<?php
namespace backend\models;

use common\models\Student;
use common\models\Class_computer;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use Yii;

/**
 * Signup form
 * Form model to enforce validation rules or other methods
 * data comes in from a controller
 */
class StudentCreate extends Model
{
    public $username;
    public $email;
    public $password;
    public $role_id;
    public $class_nr;
    public $created_at;
    public $ended_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\Student', 'message' => 'This username has already been taken.'],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'match','pattern'=>'/[0-9]{11}$/','message'=>'national number should be 11 numbers'],
            ['username', 'string', 'min' => 10, 'max' => 11],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 50],
            ['email', 'unique', 'targetClass' => '\common\models\Student', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            
            ['role_id', 'required'],
            ['role_id', 'string', 'min' => 2],
            
            ['created_at', 'required'],
            ['ended_at', 'required'],
            
            ['class_nr', 'required'],
            ['class_nr', 'string', 'max' => 10],
        ];
    }


     public function signup()
    {
            $user = new Student();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->username);
            $user->user_type_id = 10;
            $user->role_id = $this->role_id;
            $user->created_at = $this->created_at;
            $user->ended_at = $this->ended_at;
            $user->class_nr = $this->class_nr;
        if ($user->save()) {
        $auth = Yii::$app->authManager;
        $authorRole = $auth->getRole($user->getRoleName());
        $auth->assign($authorRole, $user->getId());
            return $user;
        } else {
            return $user;
        }
    }
    
      public static function getClassList()
    {
    $droptions = Class_computer::find()->asArray()->all();
    return Arrayhelper::map($droptions, 'class_id', 'class_describe');
    }
    
}
