<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\StudentEquipmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Student Equipments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-equipment-index wrapper">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app','Add a device'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'renter_id',
              [ 
            'label' => Yii::t('app','Laptop'),
            'attribute'=>'serial_id',
            'value'=>'serial.computer_name',
            ],
             [    
                 'label' => Yii::t('app','Lap status'),
                 'attribute'=>'computer_status',
                 'value'=>'serial.computer_status',
            ],
             [
                'label' => Yii::t('app','Application status'),
                'attribute' => 'borrow_status_id',
                'value' => 'status.borrow_status_name',
            ],
             [
                'label' => Yii::t('app','Application sendt by'),
                'attribute' => 'require_by_teacher_id',
                 'value' => 'teacher.username',
            ],
            [
                'label' => Yii::t('app','Rent from:'),
                'attribute' => 'start_at',
                    ],
           [
                'label' => Yii::t('app','Rent Until:'),
                'attribute' => 'end_at',
                    ],
            ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{view}{update}{delete}',
                            'buttons'=>[
                              'view' => function ($url, $model) {     
                                return Html::a('<span class=" btn-rounded btn-info glyphicon  glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('yii', 'view'),
                                ]);                                
                              }
                          ]              
             ],
        ],
          'options' => [
            'class' => 'col-sm-12',
        ],
        'export' => false,
  
    ]); ?>

</div>
