<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Studentcreate */

$this->title = Yii::t('app', 'Create a student/empolyee');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-create wrapper">
    <h1><strong><?= Html::encode($this->title) ?></strong></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
