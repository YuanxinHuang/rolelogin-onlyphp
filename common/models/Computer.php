<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * This is the model class for table "computer".
 *
 * @property string $serial_id
 * @property string $computer_status
 * @property string $model
 * @property string $computer_name
 * @property string $shelf_placement
 * @property string $purchase_date
 * @property string $warranty_date
 * @property string $end_dato
 *
 * @property ComputerDamage $computerDamage
 */
class Computer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'computer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serial_id'], 'required'],
            ['serial_id', 'unique', 'targetClass' => '\common\models\Computer', 'message' => 'This serial number has already been taken.'],
            [['purchase_date', 'warranty_date', 'end_dato'], 'safe'],
            [['serial_id'], 'string', 'max' => 25],
            [['computer_status', 'model', 'computer_name'], 'string', 'max' => 20],
            [['shelf_placement'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'serial_id' => Yii::t('app', 'Serial ID'),
            'computer_status' => Yii::t('app', 'Computer Status'),
            'model' => Yii::t('app', 'Model'),
            'computer_name' => Yii::t('app', 'Computer Name'),
            'shelf_placement' => Yii::t('app', 'Shelf Placement'),
            'purchase_date' => Yii::t('app', 'Purchase Date'),
            'warranty_date' => Yii::t('app', 'Warranty Date'),
            'end_dato' => Yii::t('app', 'End Dato'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComputerDamage()
    {
        return $this->hasOne(ComputerDamage::className(), ['serial_id' => 'serial_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvailabel() //Relation which is availabel
   {
       return $this->hasMany(common\models\Student::className(), ['id' => 'id']);
   }
   /**
    * Relation to model class
    * @return \yii\db\ActiveQuery
    */
     public function getModelType() 
   {
       return $this->hasOne(Laptopmodel::className(), ['model_name'=>'model']);
   }
   
     public function getModeltypeName()
    {
    return $this->modelType ? $this->modelType->model_id : '- no role -';
    }

   
     public function getModelTypeLink()
    {
        $url = Url::to(['laptopmodel/view', 'id'=>$this->modeltypeName]);
    $options = [];
    return Html::a($this->model, $url, $options);
    }
}
