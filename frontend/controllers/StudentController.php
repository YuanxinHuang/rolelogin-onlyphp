<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Student;
use frontend\models\ChangePasswordForm;
use common\models\Student_equipment_create;
use common\models\Student_equipment;
use common\models\StudentEquipmentConfirmForm;
use backend\models\StudentCreate;
//use backend\models\StudentUpload;
use common\models\StudentSearch;
use common\models\StudentEquipmentSearch;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends Controller
{
    public function behaviors()
    {
        return [
              'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['checkout','index','mypage','update','changepassword','view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Student model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Student model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StudentCreate();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->signup()){
//                if (Yii::$app->getUser()->login($user)) {
                    Yii::$app->session->setFlash('success', 'You had added an user.');
//                }
            }
            else {
//                $this->render('create', [
//                'model' => $model,
//                ]);
                Yii::$app->session->setFlash('errors', 'Data is not registed ');
            }
     }
    return $this->render('create', [
                'model' => $model]);
    }
    
      /**
     * Upload an existing Student model.
     * If upload is successful
     * @param integer $id
     * @return mixed
     */
     public function actionUploadform()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->upload()) {
                    // file is uploaded successfully
                 Yii::$app->session->setFlash('success', 'You had added a USERS file.');
        }           
        return $this->redirect(['upload']);
            }
    return $this->render('upload', ['model' => $model]);
    }
    
       /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    
     /**
     * checkoutbox an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionCheckout() {
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (Yii::$app->request->ispost) {
            if (Yii::$app->request->post('selection')) {  
            $selection = (array) Yii::$app->request->post('selection');
            $selectionTostring = implode(",", $selection);
            $error = [];
//            those students ID: '.$selectionTostring.'. please add the time period of rent');
            foreach ($selection as $key) {
            $model = new Student_equipment();
                if($model->requestRent($key))
                {
                  $model->requestRent($key);
                  return;
                } else {
                $error[] = $key;
                }
            }
            if(count($error)==0){
                Yii::$app->session->setFlash('success', 'YOU had add these person with ids: '.$selectionTostring);
            }
            else {
                $errorTostring = implode(",", $error);
                Yii::$app->session->setFlash('error', 'YOU cannot add these person with ids: '.$errorTostring);
            }
            $searchModel = new StudentEquipmentSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('/student_equipment/confirm', [
               'dataProvider'=>$dataProvider,
               'searchModel'=>$searchModel,
            ]);
            }
            else {Yii::$app->session->setFlash('error',  ' please add at least one student');
            return $this->render('checkbox', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
            }
        } else {
            return $this->render('checkbox', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        }
    }
    
//      public function actionAdd_rent_period(){
//          if (Yii::$app->request->ispost) {
//              } else {
//            return $this->render('add_peroid', [
//                        'selection' => $selection,
//            ]);
//        }
//      }

    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        
        if (Yii::$app->user->id==$id){
            $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['mypage']);
            } else {
                return $this->render('updateEmail', [
                    'model' => $model,
                ]);
            }
        } else {
            $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['index']);
            } else {
            return $this->render('updatestatus', [
                'model' => $model,
                ]);
            }
        }
    }
    
    public function actionChangepassword(){
        $id=Yii::$app->user->id;
        $_user = $this->findModel($id);
        $model= new ChangePasswordForm();
            if ($model->load(Yii::$app->request->post())) {
                if($_user->validatePassword($model->password_old)){
                    if($model->setnewPassword($_user) ){
                    Yii::$app->session->setFlash('success',  'You had changed your password');
                    return $this->redirect(['mypage']);
                    } else {
                    Yii::$app->session->setFlash('error',  'You had not changed your password');
                    return $this->redirect(['mypage']);}
                } else {
                    Yii::$app->session->setFlash('error', 'Your old password is not correct, please enter again');
                    return $this->redirect(['changepassword']);
                }
            } else {
            return $this->render('changepassword', ['model' => $model,
                ]);
            }
    }
    

    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
      /**
     * view only my page.
     * @param integer $id
     * @return mixed
     */
    
      public function actionMypage() {
         return $this->render('mypage', [
            'model' => $this->findModel(Yii::$app->user->id),
        ]);
    }
    


    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
