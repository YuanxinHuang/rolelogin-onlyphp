<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "class_computer".
 *
 * @property string $class_id
 * @property string $class_describe
 * @property integer $allow_computer_type
 */
class Class_computer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'class_computer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_id', 'class_describe', 'allow_computer_type'], 'required'],
            [['allow_computer_type'], 'integer'],
            [['class_id'], 'string', 'max' => 20],
            [['class_describe'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'class_id' => Yii::t('app', 'Class ID'),
            'class_describe' => Yii::t('app', 'Class Description'),
            'allow_computer_type' => Yii::t('app', 'Allowed Computer Type'),
        ];
    }
     /**
     * @get computer name
      * @return string
     */
    
}
