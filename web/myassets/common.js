jQuery(document).ready(function() {

    var oTable1 = $('#sample_1').dataTable({
        columnDefs: [
            {
                orderable: false,
                targets: -1
            }
        ],
        stateSave: true
    });
    var allPages1 = oTable1.fnGetNodes();

    var oTable2 = $('#sample_2').dataTable({
        columnDefs: [
            {
                orderable: false,
                targets: -1
            }
        ]
    });
    var allPages2 = oTable2.fnGetNodes();
    var oTable3 = $('#sample_3').dataTable({
        columnDefs: [
            {
                orderable: false,
                targets: -1
            }
        ]
    });
    var allPages3 = oTable3.fnGetNodes();

    $( "#student_total").click(function(){
        //var classname = $("#student_total").attr("class");
        var checked = $("#student_total").is(':checked');
        $('input[type="checkbox"]', allPages1).prop('checked', checked);
    });

    $( "#teacher_total").click(function(){
       // var classname = $("#teacher_total").attr("class");
        var checked = $("#teacher_total").is(':checked');
        //checkAll(classname, checked);
        $('input[type="checkbox"]', allPages2).prop('checked', checked);
    });

    $( "#department_total").click(function(){
       // var classname = $("#department_total").attr("class");
        var checked = $("#department_total").is(':checked');
       // checkAll(classname, checked);
        $('input[type="checkbox"]', allPages3).prop('checked', checked);
    });

});


function checkAll(classname , checked)
{
       var valueToApply = false;
       if(checked == true)
       {
            valueToApply = true;
       }else
            valueToApply = false;

         $("."+ classname).prop('checked', valueToApply);


}