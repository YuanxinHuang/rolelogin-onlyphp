<?php
/**
* @link http://www.yiiframework.com/
* @copyright Copyright (c) 2008 Yii Software LLC
* @license http://www.yiiframework.com/license/
*/
namespace common\models;

use Yii;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;
use yii\helpers\Security;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\models\Role;
use backend\models\Status;
use backend\models\StudentType;
use common\models\Class_computer;
/**
 * This is the model class for table "student".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role_id
 * @property integer $user_type_id
 * @property integer $status_id
 * @property integer $created_at
 * @property integer $ended_at
 */
class Student extends ActiveRecord implements IdentityInterface{
    
    const STATUS_ACTIVE = 10;
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'system_user';
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['username'], 'required'],
            [['phone','role_id', 'user_type_id', 'status_id', 'created_at', 'ended_at'], 'integer'],
            [['password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['username'], 'string', 'min' => 10, 'max' => 11],
            ['username', 'match','pattern'=>'/[0-9]{11}$/','message'=>'national number should be 11 numbers'],
            ['username', 'unique', 'targetClass' => '\common\models\Student', 'message' => 'This national number has already been taken.'],
            [['auth_key'], 'string', 'max' => 32],
            [['class_nr'], 'string', 'max' => 20],
            ['username', 'filter', 'filter' => 'trim'],
            [['realname'], 'string', 'max' => 40],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'unique'],
            ['status_id', 'default', 'value' => self::STATUS_ACTIVE],
            [['status_id'],'in', 'range'=>array_keys($this->getStatusList())],
            ['role_id', 'default', 'value' => 10],
            [['role_id'],'in', 'range'=>array_keys($this->getRoleList())],
            ['user_type_id', 'default', 'value' => 10],
            [['user_type_id'],'in', 'range'=>array_keys($this->getUserTypeList())],
          
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'phone' => Yii::t('app', 'Phone'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'role_id' => Yii::t('app', 'Role ID'),
            'user_type_id' => Yii::t('app', 'User Type ID'),
            'status_id' => Yii::t('app', 'Status ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'ended_at' => Yii::t('app', 'Ended At'),
            'class_nr' => Yii::t('app', 'Class Nr'),
            'realname' => Yii::t('app', 'Realname'),
        ];
    }
    
    /**
    * @findIdentity  Identity Methods
    */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status_id' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status_id' => self::STATUS_ACTIVE]);
    }
   

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status_id' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserEquipment()
    {
        return $this->hasOne(UserEquipment::className(), ['renter_id' => 'id']);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    /**
    * Soft delete en student
    *
    */
    public function softDelete(){
        $this->status_id = 99;
    }
    
     /**
    * recreate en student
    *
    */
    public function recreate(){
        $this->status_id = 10;
    }
    
    /**
    * get role in system
    *
    */
    public function getRole()
    {
    return $this->hasOne(Role::className(), ['role_value' => 'role_id']);
    }
    
    public function getRoleName()
    {
    return $this->role ? $this->role->role_name : '- no role -';
    }
    
     /**
    * get Student type in system
    *
    */
    public function getStudentType()
    {
    return $this->hasOne(StudentType::className(), ['student_type_value' => 'user_type_id']);
    }
    
    public function getStudentTypeName()
    {
    return $this->studentType ? $this->studentType->student_type_name : '- not a student -';
    }
   
    // if we want this function to work , add use yii\helpers\ArrayHelper;
    // and uncomment the one of rule about getrolelist
    //this model only to accept role_id values that are in the range of the
    // role_value in the role table
    public static function getRoleList()
    {
        //this is good function when we want a angular multi select 
    $droptions = Role::find()->asArray()->all();
    return Arrayhelper::map($droptions, 'role_value', 'role_name');
    }
    public static function getAvailableStudent(){
    $droptions = Student::find()->where($condition, $params);
    return Arrayhelper::map($droptions, 'role_value', 'role_name');
    }
    
    
    public static function getGraduatedUser($lower)
{
    return Student::find()
        ->where(['<=', 'created_at', $lower])
        ->all();
}
      /**
    * get class in system
    *
    */
    public function getClass()
    {
    return $this->hasOne(Class_computer::className(), ['class_id' => 'class_nr']);
    }
    
    public function getClassName()
    {
    return $this->class ? $this->class->class_describe : '- no class -';
    }
    
     public function getAllowedComputerName()
    {
    return $this->allowedComputer ? $this->allowedComputer->computer_type_name : '- no class -';
    }
    
     public function getAllowedComputer()
    {
    return $this->hasOne(Computer_type::className(), ['computer_type_value' => 'allow_computer_type'])
                    ->viaTable('class_computer', ['class_id' => 'class_nr']);
    }
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
    return $this->hasOne(Status::className(), ['status_value' => 'status_id']);
    }
    
    public function getStatusName()
    {
    return $this->status ? $this->status->status_name : '- no status -';
    }
   
     /**
     * @return \yii\db\ActiveQuery
     * get list of statuses for dropdown
     */
         
    public static function getStatusList()
    {
    $droptions = Status::find()->asArray()->all();
    return Arrayhelper::map($droptions, 'status_value', 'status_name');
    }
    
    public function getUserType()
    {
    return $this->hasOne(StudentType::className(), ['student_type_value' =>
    'user_type_id']);
    }
    
    public function getUserTypeName()
    {
    return $this->userType ? $this->userType->student_type_name : '- no user type -';
    }
    
    
    public static function getUserTypeList()
    {
    $droptions = StudentType::find()->asArray()->all();
    return Arrayhelper::map($droptions, 'student_type_value', 'student_type_name');
    }
    
    // use this function usertype above
    public function getUserTypeId()
    {
    return $this->userType ? $this->userType->id : 'none';
    }
    
    public function getProfile()
    {
    return $this->hasOne(Student_equipment::className(), ['renter_id' => 'id']);
    }

    public function getProfileId()
    {
    return $this->profile ? $this->profile->id : 'none';
    }

    public function getProfileLink()
    {
    $url = Url::to(['StudentEquipment/view', 'id'=>$this->profileId]);
    $options = [];
    return Html::a($this->profile ? 'profile' : 'none', $url, $options);
    }

    public function getStudentIdLink()
    {
    $url = Url::to(['student/update', 'id'=>$this->id]);
    $options = [];
    return Html::a($this->id, $url, $options);
    }

    public function getStudentEmail()
    {
        $url = Url::to(['student/view', 'id'=>$this->id]);
    $options = [];
    return Html::a($this->email, $url, $options);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComputerDamage()
    {
        return $this->hasOne(ComputerDamage::className(), ['damage_user_id' => 'id']);
    }

    
    /**
     * @return \yii\db\ActiveQuery
     * get list of statuses for dropdown
     */
         
    public static function getClassList()
    {
    $droptions = Class_computer::find()->asArray()->all();
    return Arrayhelper::map($droptions, 'class_id', 'class_describe');
    }
    

}