<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-form">
    <?php error_reporting(E_ALL); 
    $form = ActiveForm::begin([
    'id' => $model->formName(),
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['deviceSize' => ActiveForm::SIZE_SMALL],
]); ?>
    <?= $form->field($model, 'username',[
    'hintType' => ActiveField::HINT_SPECIAL,
    'hintSettings' => ['placement' => 'right', 
        'onLabelClick' => true, 
        'onLabelHover' => false]])
        ->hint(Yii::t('app','Please enter a new user´s person number.'))
        ->label('National number')
        ->textInput(['maxlength' => true])?>
    
    <?= $form->field($model, 'email',['labelOptions'=>['class'=>'col-sm-2 col-md-2']])
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role_id')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(\backend\models\Role::find()->all(),'role_value','role_name'),
    'options' => ['placeholder' => 'Select a role type ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ])->label(Yii::t('app', 'User is')) ?>
   
    <?= $form->field($model, 'class_nr')->widget(Select2::classname(), [
    'data' => $model->getClassList(),
    'options' => ['placeholder' => 'Select a Class name ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ])->label(Yii::t('app','Class')) ?>
    <?php 
    echo '<label class="control-label">Valid Dates</label>';
    echo DatePicker::widget([
    'model' => $model,
    'attribute' => 'created_at',
    'attribute2' => 'ended_at',
    'options' => ['placeholder' => Yii::t('app','Start date of school')],
    'options2' => ['placeholder' => Yii::t('app','End date of school')],
    'type' => DatePicker::TYPE_RANGE,
    'form' => $form,
    'pluginOptions' => [
        'format' => 'yyyymmdd',
        'autoclose' => true,
    ]
]);?>

<div>
    <hr>
    <?= Html::submitButton(Yii::t('app', 'submit'), ['class' => 'btn btn-success']) ?>
    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
</div>
    <?php ActiveForm::end(); ?>
</div>
<!--
//$script = <<< JS
//
//$('form#{$model->formName()}'.on('beforeSubmit',function(e)
//{
//    var \$form = $(this);
//    $.post(
//        \$form.attr("action"),
//        \$form.serialize()
//        )
//        .done(function(result){
//            if(result == 1)
//            {
//                $(\$form).trigger("reset");
//                $.pjax.reload({container:'#studentGrid'});
//            } else 
//            {
//                $("#studentGrid").html(result);
//            }
//        }).fail(function(){
//           console.log("server error");
//   });
//return false;     
//   });
//JS;
//$this->registerJs($script);
-->
