<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
/* @var $this yii\web\View */
/* @var $model common\models\Student */
$this->title = 'Update My Password';
$this->params['breadcrumbs'][] = Yii::t('app', 'Update password');
?>
<div class="student-update">

  <div class="student-form col-md-offset-3 col-md-6 wrapper">
      <h1><?= Html::encode($this->title) ?></h1>
      <p>Du ber om tilgang til sensitiv informasjon, så må du bekrefte passordet.</p>
    <?php $form = ActiveForm::begin(); ?>
 
   <?= $form->field($model, 'password_old',[
    'hintType' => ActiveField::HINT_SPECIAL,
    'hintSettings' => ['placement' => 'left', 'onLabelClick' => true, 'onLabelHover' => true]
])->hint('Hvis du har glemt passordet ditt, kan du tilbakestille passordet ved å gå til "Logg inn" siden. Der finner du veiledning for hvordan du kan få nytt passord til din Epost. ')->passwordInput(['placeholder'=>'Enter your current password...']);?>
     
   <?= $form->field($model, 'password_new')->passwordInput(['placeholder'=>'Enter your new password...']) ?>
   <div class="form-group">
      <?= Html::submitButton(Yii::t('app', 'submit'), ['class' => 'btn btn-success']) ?>
   </div>
    <?php ActiveForm::end(); ?>

</div>

</div>
