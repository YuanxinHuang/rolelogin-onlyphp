<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Admin2 */

$this->title = Yii::t('app', 'Create Admin2');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admin2s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin2-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
