<?php
namespace backend\models;

use common\models\Admin;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class ChangePasswordForm extends Model
{
    public $password_new;
    public $password_old;

    /**
     * @var \common\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password_new', 'required'],
            ['password_old', 'required'],
            ['password_new', 'string', 'min' => 6],
            ['password_old', 'string', 'min' => 6],
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function setnewPassword($user)
    {
        $user->setPassword($this->password_new);
        return $user->save(false);
    }
    
}
