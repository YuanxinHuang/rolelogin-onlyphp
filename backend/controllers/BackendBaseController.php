<?php
namespace backend\controllers;
use Yii;
use yii\web\Controller;


/**
 * Description of BackendBaseController
 *
 * 
 */


class BackendbaseController extends Controller
{public $layout  = '/main';
public function beforeAction($action)
{
    if (empty(Yii::$app->user->getId())) {
        Yii::$app->controller->redirect(['/site/login']);
        return false;
    }
    //判断权限
    return true;
}
public function actions()
{
    return [
        'error' => [
            'class' => 'yii\web\ErrorAction',
        ],
    ];
}
public function actionError()
{
    $this->layout = '@backend/views/layouts/main.php';
    if($error=Yii::app()->errorHandler->error){
        $this->render('error', $error);
    }
}
}