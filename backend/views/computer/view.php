<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Computer */

$this->title = $model->serial_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Computers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="computer-view wrapper">
     <?= Html::a(Yii::t('app', 'Back'), ['index'], [
            'class' => 'btn btn-default pull-right',
        ]) ?>
    <?= DetailView::widget([
        'model' => $model,
        'hover'=>true,
        'mode' => DetailView::MODE_VIEW,
        'attributes' => [
            'serial_id',
            'computer_status',
            'model',
            'computer_name',
            'shelf_placement',
            'purchase_date',
            'warranty_date',
            'end_dato',
        ],
        'panel' => [
        'heading'=>'<img src="'.Yii::$app->request->BaseUrl.'/assets/images/kommuneimage.png" '
        . 'alt="service invoice" style="height:128px;">'
        .'<h1>Computer info</h1>',
        ],
        'buttons1'=> '',
    ]) ?>

</div>
