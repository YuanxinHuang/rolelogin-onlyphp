<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Laptopmodel;

/**
 * Laptopmodel_search represents the model behind the search form about `common\models\Laptopmodel`.
 */
class Laptopmodel_search extends Laptopmodel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id', 'computer_type_id'], 'integer'],
            [['model_name', 'cpu', 'ram', 'storage_capacity'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Laptopmodel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'model_id' => $this->model_id,
            'computer_type_id' => $this->computer_type_id,
        ]);

        $query->andFilterWhere(['like', 'model_name', $this->model_name])
            ->andFilterWhere(['like', 'cpu', $this->cpu])
            ->andFilterWhere(['like', 'ram', $this->ram])
            ->andFilterWhere(['like', 'storage_capacity', $this->storage_capacity]);

        return $dataProvider;
    }
}
