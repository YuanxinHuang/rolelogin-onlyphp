<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index wrapper">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'username',
             'email:email',
             'role_id',
             'user_type_id',
             'status.status_name',
             'created_at',
             'class_nr',
               ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{update}',
                            'buttons'=>[
                              'view' => function ($url, $model) {     
                                return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, [
                                        'title' => Yii::t('yii', 'update'),
                                ]);                                
            
                              }
                          ]                            
                ],
           
        ],
                                   'options' => [
            'class' => 'col-sm-12',
        ],
    ]); ?>

</div>
