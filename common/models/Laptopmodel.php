<?php

namespace common\models;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "model".
 *
 * @property integer $model_id
 * @property string $model_name
 * @property string $cpu
 * @property string $ram
 * @property string $storage_capacity
 * @property integer $computer_type_id
 */
class Laptopmodel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['computer_type_id'], 'required'],
            [['computer_type_id'], 'integer'],
            [['model_name'], 'string', 'max' => 20],
            [['cpu', 'ram', 'storage_capacity'], 'string', 'max' => 10],
            [['model_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('app', 'Model ID'),
            'model_name' => Yii::t('app', 'Model Name'),
            'cpu' => Yii::t('app', 'Cpu'),
            'ram' => Yii::t('app', 'Ram'),
            'storage_capacity' => Yii::t('app', 'Storage Capacity'),
            'computer_type_id' => Yii::t('app', 'Computer Type ID'),
        ];
    }
    
      public static function getLaptopTypeList()
    {
        //this is good function when we want a angular multi select 
    $droptions = Computer_type::find()->asArray()->all();
    return Arrayhelper::map($droptions, 'computer_type_value', 'computer_type_name');
    }
}
