<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Computer Damages Service Invoice');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="computer-damage-index wrapper">
    <h1><i class="fa fa-history"></i><?= Html::encode($this->title) ?></h1>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
             'pjax'=>true,
               'pjaxSettings'=>[
                'neverTimeout'=>true,
            ],
        'columns' => [
            'serial_id',
            'damage_user_id',
            'damage_explain:ntext',
//             [
//                'attribute'=>'image',
//                'value'=>$model->imageurl,
//                'format' => ['image',['width'=>'100']],
//              ],
            'registered_at',
            'repaired_date',

                 ['class' => 'yii\grid\ActionColumn',
                  'header'=>  'Download',
                  'headerOptions' => ['width' => '70'],
                          'template'=>' {view} ',
                            'buttons'=>[
                            'view' => function ($url, $model) {     
                                return Html::a('<span class=" btn-rounded btn-info glyphicon  glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('yii', 'view'),
                                ]);                                
                              }
                  ]              
             ],
        ],
        'export' => false,
    ]); ?>
<?php Pjax::end(); ?>
</div>
