<?php

namespace common\models;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use Yii;
use common\models\RecordHelpers;

/**
 * This is the model class for table "{{%user_equipment}}".
 *
 * @property integer $id
 * @property integer $renter_id
 * @property string $serial_id
 * @property integer $borrow_status_id
 * @property integer $require_by_teacher_id
 *
 * @property SystemUser $renter
 */
class Student_equipment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_equipment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['renter_id', 'borrow_status_id', 'require_by_teacher_id'], 'required'],
            [['renter_id', 'borrow_status_id', 'require_by_teacher_id'], 'integer'],
            [['serial_id'], 'string', 'max' => 25],
            [['renter_id'], 'unique'],
            [['start_at', 'end_at'], 'safe'],
            [['serial_id'], 'unique'],
            [['renter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(),
                'targetAttribute' => ['renter_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'renter_id' => Yii::t('app', 'Renter ID'),
            'serial_id' => Yii::t('app', 'Serial ID'),
            'borrow_status_id' => Yii::t('app', 'Borrow Status ID'),
            'require_by_teacher_id' => Yii::t('app', 'Requested By Teacher ID'),
            'start_at' => Yii::t('app', 'Start At'),
            'end_at' => Yii::t('app', 'End At'),
            'computer_status'=> Yii::t('app', 'Computer status'),
        ];
    }
    
    public function requestRent($key)
    {
                $newstudent = Student::findOne($key);
                if ($newstudent->getUserEquipment() == null){
                $this->renter_id = $newstudent->id;
                $this->borrow_status_id = 10;
                $this->require_by_teacher_id = Yii::$app->user->id;
                $this->start_at=time();
                $this->end_at=time();
                $this->save();
                return $newstudent;
                } else 
                return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getRenter()
//    {
//        return $this->hasOne(Student::className(), ['id' => 'renter_id']);
//    }
    
      public function getRenterList()
    {
          $droptions = Student::find()->asArray()->all();
    return Arrayhelper::map($droptions, 'id', 'username');
    }
    
    
     public function getTeacher()
    {
        return $this->hasOne(Student::className(), ['id' => 'require_by_teacher_id']);
    }
    
       public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'renter_id']);
    }
    
    /**
    * @return \yii\db\ActiveQuery
    * get status relation
    */
    public function getStatus()
    {
    return $this->hasOne(BorrowStatus::className(), ['borrow_status_value' => 'borrow_status_id']);
    }
    
    public function getStatusName()
    {
    return $this->status ? $this->status->borrow_status_name : '- no status -';
    }
   
//    get list of statuses for dropdown
    public static function getStatusList()
    {
    $droptions = BorrowStatus::find()->asArray()->all();
    return Arrayhelper::map($droptions, 'borrow_status_value', 'borrow_status_name');
    }
    
     /**
    * @return \yii\db\ActiveQuery
    * get computer
    */

    public function getSerial()
    {
        return $this->hasOne(\common\models\Computer::className(), ['serial_id' => 'serial_id']);
    }

     public static function getEquipmentList()
    {
        $droptions = \common\models\Computer::find()->asArray()->all();
        return Arrayhelper::map($droptions, 'serial_id', 'computer_name');
    }
    
     
}
