<aside class="main-sidebar bg-light-orange">

    <section class="sidebar">

        <!-- search form -->
<!--        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->
    <?php
    if (!Yii::$app->user->isGuest){
        echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                 
                    ['label' => Yii::t('app','Damage'), 
                        'icon' => 'glyphicon glyphicon-floppy-remove', 
                        'url' => ['#'],
                        'options' => ['class' => 'sidebar-menu'],
                         'items' => [
                            ['label' => Yii::t('app', 'All Service lapp'), 'icon' => 'fa fa-download', 'url' => ['/computerdamage/damagepdf_list'],],
                             ['label' => Yii::t('app', 'All damage'), 'icon' => 'glyphicon glyphicon-floppy-remove', 'url' => ['/computerdamage'],],
                             ['label' => Yii::t('app', 'New damages'), 'icon' => 'fa fa-plus', 'url' => ['/computerdamage/create'],]
                            ],
                         ],
                     ['label' => Yii::t('app','Users'), 
                        'icon' => 'fa fa-users', 
                        'url' => ['#'],
                        'options' => ['class' => 'sidebar-menu'],
                         'items' => [
                            ['label' => Yii::t('app', 'All Users'), 'icon' => 'fa fa-users', 'url' => ['/student'],],
                            ['label' => Yii::t('app', 'New User'), 'icon' => 'fa fa-user', 'url' => ['/student/create'],]
                            ],
                         ],
                    
                    ['label' => Yii::t('app', 'Laptop Loans'),
                        'icon' => 'fa fa-list-alt', 
                        'url' => ['#'],
                        'options' => ['class' => 'sidebar-menu'],
                            'items' => [
                            ['label' => Yii::t('app', 'Add a Loan'), 'icon' => 'fa fa-plus', 'url' => ['/studentequipment/create'],],
                            ['label' => Yii::t('app', 'Add Many Loan'), 'icon' => 'fa fa-plus', 'url' => ['/student/checkout'],],
                            ['label' => Yii::t('app','Loans Overview'), 'icon' => 'fa fa-list-alt', 'url' => ['/studentequipment/index'],],
                        ],
                    ],
                    
                       ['label' => Yii::t('app', 'Laptop'),
                            'icon' => 'fa fa-laptop', 
                            'url' => ['#'],
                            'options' => ['class' => 'sidebar-menu'],
                                'items' => [
                                ['label' => Yii::t('app','Laptop Overview'), 'icon' => 'fa fa-dashboard', 'url' => ['/computer/index'],],
                                ['label' => Yii::t('app', 'Register New Laptop'), 'icon' => 'fa fa-plus', 'url' => ['/computer/create'],],
                               
                            ],
                        ],   
                      ['label' => Yii::t('app', 'Laptop Models'),
                            'icon' => 'fa fa-dashboard', 
                            'url' => ['#'],
                            'options' => ['class' => 'sidebar-menu'],
                                'items' => [
                                ['label' => Yii::t('app', 'Register New Models'), 'icon' => 'fa fa-plus', 'url' => ['/laptopmodel/create'],],
                                ['label' => Yii::t('app','Models Overview'), 'icon' => 'fa fa-dashboard', 'url' => ['/laptopmodel/index'],],
                            ],
                        ], 
                        ['label' => Yii::t('app', 'Excel registration'),
                        'icon' => 'fa fa-file-excel-o', 
                        'url' => ['/student/uploadform'],
                        'options' => ['class' => 'sidebar-menu'],
                        ],
                           
                             ['label' => Yii::t('app', 'Change My Password'),
                        'icon' => 'fa fa-user', 
                        'url' => ['/admin_user/changepassword'],
                        'options' => ['class' => 'sidebar-menu'],
                        ],
                         ['label' => Yii::t('app', 'Student/Employee Password'),
                        'icon' => 'fa fa-unlock-alt', 
                        'url' => ['/student/passwordlist'],
                        'options' => ['class' => 'sidebar-menu'],
                        ],
                    
                   
                  
                ],
            ]
        ); 
    }?>

    </section>

</aside>
