<?php
namespace common\models;
use common\models\Student;

/**
 * Description of this Class: Not implement yet
 *
 * @author 
 */
        class StudentEquipmentConfirmForm extends \yii\db\ActiveRecord {
    public $checkbox_field; //A custom variable to hold the value of checkboxlist
   
    public function rules()
    {
        return [
            [['checkbox_field'], 'safe']
        ];
    }

    public function getStudents() //Relation between ticket & ticket_complain table
    {
        return $this->hasMany(common\models\Student::className(), ['id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes){
        foreach ($this->complains_field as $id) { 
            $tc = new common\models\Student();
            $tc->ticket_id = $this->id;
            $tc->complain_id = $id;
            $tc->save();
        }
    }
}

