<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ComputerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Computers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="computer-index wrapper">

    <h1><?= Html::encode($this->title) ?></h1>
        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Create Computer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['timeout' => 3000]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'serial_id',
            'computer_status',
            'computerType.computer_type_value',
            'model',
            'computer_name',
             'cpu',
             'ram',
             'storage_capacity',
             'shelf_placement',
             'purchase_date',
             'warranty_date',
          

            ['class' => 'yii\grid\ActionColumn'],
        ],
          'options' => [
            'class' => 'col-sm-12',
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>
