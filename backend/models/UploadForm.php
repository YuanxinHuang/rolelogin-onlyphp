<?php 
namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile file attribute
     */
    public $file;

    /**
     * @return array the validation rules.
     */
  public function rules()
{
    return [
        [['file'], 'required'],
        [['file'], 'file'],
    ];
}

public function upload()
    {   
    $path = 'uploads/';
    if (!file_exists($path)) { 
            @mkdir($path, 0777, TRUE); 
            @chmod($path, 0777);
        }
        if ($this->validate()) {
            if ($this->file){
                if (!strpos($this->file->type,'sheet') === false){
                    if ($this->file->saveAs($path.'test.'.$this->file->extension)){
                    return true;
                    }
                    else {
                        throw new NotFoundHttpException('You had not chosen any file.');
                    }
                }
            } else {
                    throw new NotFoundHttpException('You had not chosen any file.');
            }
        }
        else {
            return false;
        }
    }
    
    public function uploadimage()
    {
        if ($this->validate()) {  
            if ($this->file){
                if ($this->file->saveAs('uploads/' . $this->file->name)){
                return true;
                }
                else {
                    throw new NotFoundHttpException('You had not chosen any image.');
                }
            } else {
                    throw new NotFoundHttpException('You had not chosen any image.');
            }
        }
    }
    
    public function getName(){
        return $this->file->name;
    }
}