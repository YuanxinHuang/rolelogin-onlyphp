<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ComputerType;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Computer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="computer-form">

    <?php $form = ActiveForm::begin([
            'options' => [
            'class' => 'col-md-6',
        ],
            ]); ?>

    <?= $form->field($model, 'serial_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_id')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(ComputerType::find()->all(),'computer_type_value','computer_type_name'),
    'language' => 'no',
    'options' => ['placeholder' => 'Select a computer type ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ]) ?>

    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'computer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cpu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ram')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'storage_capacity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shelf_placement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'purchase_date')->widget(DatePicker::className(),[
    'name' => 'purchase_date', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>

    <?= $form->field($model, 'warranty_date')->widget(DatePicker::className(),[
    'name' => 'warranty_date', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>
    
    <?= $form->field($model, 'end_dato')->widget(DatePicker::className(),[
    'name' => 'end_dato', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>

    <div class="form-group">
        <!-- Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])--> 
    <?= Html::submitButton('submit', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
