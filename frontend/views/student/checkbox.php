<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class = "wrapper-checkout wrapper">
    <h1>Choose who want to borrow</h1>
<!--['controller/bulk'],'post'-->
<?= Html::beginForm();?>

<?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'col-md-12',
            'id' => 'grid',
        ],
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'username',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
             [
                'label' => 'Click here for more infor:',
                'attribute'=>'email',
                'value'=> 'studentEmail',
                'format' => 'raw',
              ],
            //'status_id',
              [
                'label' => 'Started school at:',
                'attribute'=>'created_at',
                'value'=>'created_at',
                'format' =>'datetime',
             ],
            // 'ended_at',
            'class_nr',
            ['class' => 'yii\grid\CheckboxColumn'],
        ],
    'export' => false,
    ]); ?>
<?= Html::submitButton('Send requests to IKT department', [
    'class' => 'btn btn-primary',
    ]);?>
<?= Html::endForm();?> 
</div>


