<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Admin2 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Admin2',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admin2s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="admin2-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
