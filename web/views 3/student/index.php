<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

    $this->title = Yii::t('app', 'Users');
    $this->params['breadcrumbs'][] = $this->title;

?>
  <?php
    Modal::begin([
    'id'=>'modalC',
    'size'=> 'modal-md',
    ]);
    $newmodelCreate = new backend\models\StudentCreate();

    echo $this->render('create', [
        'model' => $newmodelCreate,
    ]);
    Modal::end();?>
    
    <div class="inline wrapper">
        <h1><i class="fa fa-flag-o-red"></i><strong>Registered Users</strong></h1>
      
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
             'pjax'=>true,
               'pjaxSettings'=>[
                'neverTimeout'=>true,
                'beforeGrid'=>'My fancy content before.',
                'afterGrid'=>'My fancy content after.',
            ],
            'columns' => [
                'id',
                'username',
                 'email:email',
                 'role.role_name',
                 'status.status_name',
//                [
//                  'attribute'=>'status',
//                 'filter'=>array("10"=>"ACTIVE","20"=>"FEE NOT PAID"),
//                 ],
                [
                'label' => Yii::t('app', 'This person is:'),
                'attribute'=>'user_type_id',
                'value'=>'studentTypeName',
                ],
                 ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{view} {update} {delete}{recover}',
                            'buttons'=>[
                              'view' => function ($url, $model) {     
                                return Html::a('<span class=" btn-rounded btn-info glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('yii', 'view'),
                                ]);                            
                              },
                               'update' => function ($url, $model) {     
                                return Html::a('<span class=" btn-rounded btn-primary glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('yii', 'update'),
                                ]);                                
                              },
                                      
                                'delete' => function ($url, $model) {     
                                return Html::a('<span class=" btn-rounded btn-primary glyphicon glyphicon-remove"></span>', $url, [
                                        'title' => Yii::t('yii', 'delete'),
                                ]);                            
                              },
                                'recover' => function ($url, $model) {     
                                return Html::a('<i class=" btn-rounded btn-info fa fa-history"></i>', $url, [
                                        'title' => Yii::t('yii', 'recover'),
                                ]);                            
                              },
                          ]              
             ],
            ],
        'panel' => [
        'before'=>Html::button(Yii::t('app', 'Register new'),[ 'id' =>'modal_student_create_button' ,'value'=> Url::to('student/create'), 'class'=>'btn btn-success']),
        'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i>' .Yii::t('app', 'Reset Grid'), ['index'], ['class' => 'btn btn-info']),
        'footer'=>false
    ],
        'export' => false,
        ]); ?>
   
    </div>
</div>
