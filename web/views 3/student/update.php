<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Student */

$this->title = 'Update '.$model->role->role_name.' User: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-update">

    <h1><?= Html::encode($this->title) ?></h1>

  <div class="student-form">

    <?php error_reporting(E_ALL); 
    $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['deviceSize' => ActiveForm::SIZE_SMALL],
]); ?>
    <?= $form->field($model, 'username',['labelOptions'=>['class'=>'col-sm-2 col-md-2']])->textInput(['maxlength' => true])
            ->hint('Please enter the national number')->label('National number')  ?>
    
    <?= $form->field($model, 'email',['labelOptions'=>['class'=>'col-sm-2 col-md-2']])->textInput(['maxlength' => true]) ?>
   
    <?= $form->field($model, 'class_nr')->widget(Select2::classname(), [
    'data' => $model->getClassList(),
    'options' => ['placeholder' => 'Select a Class name ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ])->label('Class') ?>

    <?php 
    echo '<label class="control-label">Valid Dates</label>';
    echo DatePicker::widget([
    'model' => $model,
    'attribute' => 'created_at',
    'attribute2' => 'ended_at',
    'options' => ['placeholder' => 'Start date of school'],
    'options2' => ['placeholder' => 'End date of school'],
    'type' => DatePicker::TYPE_RANGE,
    'form' => $form,
    'pluginOptions' => [
        'format' => 'yyyymmdd',
        'autoclose' => true,
    ]
]);?>

<div>
    <hr>
    <?= Html::submitButton('submit information', ['class' => 'btn btn-success']) ?>
    <?= Html::a('Add new class', 
            Yii::$app->request->baseUrl.'/admin.php/site/logout',['class' => 'btn btn-success']) ?>
</div>
    <?php ActiveForm::end(); ?>

</div>
</div>
