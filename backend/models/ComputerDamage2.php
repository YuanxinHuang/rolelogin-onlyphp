<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ComputerDamage;

/**
 * ComputerDamage2 represents the model behind the search form about `backend\models\ComputerDamage`.
 */
class ComputerDamage2 extends ComputerDamage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'damage_user_id'], 'integer'],
            [['serial_id', 'damage_explain', 'image', 'registered_at', 'repaired_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ComputerDamage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'damage_user_id' => $this->damage_user_id,
            'registered_at' => $this->registered_at,
            'repaired_date' => $this->repaired_date,
        ]);

        $query->andFilterWhere(['like', 'serial_id', $this->serial_id])
            ->andFilterWhere(['like', 'damage_explain', $this->damage_explain])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
