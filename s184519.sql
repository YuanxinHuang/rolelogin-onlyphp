-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 28. Apr, 2016 23:49 PM
-- Server-versjon: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `s184519`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `admin_user`
--

CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` smallint(6) NOT NULL DEFAULT '10',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dataark for tabell `admin_user`
--

INSERT INTO `admin_user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `role_id`, `status`, `created_at`, `updated_at`) VALUES
(5, 'admin', '', '$2y$13$zKpWMzfj0kDawj27ZV/V4Otebk5qJ7G7dKh2yY.tQzlio08lbwhq.', NULL, 'vicky.huangyuanxin@gmail.com', 40, 10, 1455997205, 1461449755),
(6, '12341234567', 'asdfasdf', '$2y$13$NNoo8LTa4VLOYQh2cWRNn.DEbgzN.Jg4x4pah1WdIq55L9b0ZLQ0q', '349BAucZt_sRVRNnk4QarBeXois96APh_1461447969', 'vicky.huangyuanxin86@gmail.com', 40, 10, 123123, 1461447969);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dataark for tabell `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('accounting_department', '406', 1459259694),
('accounting_department', '407', 1459259694),
('accounting_department', '412', 1460565997),
('accounting_department', '418', 1460596174),
('student', '389', 1459258449),
('student', '390', 1459258450),
('student', '391', 1459258450),
('student', '392', 1459258451),
('student', '408', 1459288286),
('student', '413', 1460566862),
('student', '424', 1461279279),
('student', '427', 1461366037),
('student', '428', 1461366075),
('student', '429', 1461366330),
('student', '430', 1461366330),
('student', '431', 1461366331),
('student', '432', 1461366332),
('student', '433', 1461366332),
('student', '434', 1461366333),
('student', '435', 1461366333),
('student', '436', 1461366334),
('student', '437', 1461366335),
('student', '438', 1461366335),
('student', '439', 1461366336),
('student', '440', 1461366337),
('student', '441', 1461366337),
('student', '442', 1461366338),
('student', '443', 1461366863),
('student', '444', 1461366864),
('student', '445', 1461366864),
('student', '446', 1461366865),
('student', '447', 1461366866),
('student', '448', 1461366866),
('student', '449', 1461366867),
('student', '450', 1461366868),
('student', '451', 1461366868),
('student', '452', 1461366869),
('student', '453', 1461366869),
('student', '454', 1461366870),
('student', '455', 1461366871),
('student', '456', 1461366871),
('student', '457', 1461366872),
('student', '458', 1461366873),
('student', '459', 1461366873),
('student', '460', 1461366874),
('student', '461', 1461366875),
('student', '462', 1461366875),
('student', '463', 1461366876),
('student', '464', 1461366877),
('student', '465', 1461366877),
('student', '466', 1461366878),
('student', '467', 1461366879),
('student', '468', 1461454897),
('student', '469', 1461454898),
('student', '470', 1461454899),
('student', '471', 1461454899),
('student', '472', 1461454900),
('student', '473', 1461454901),
('student', '474', 1461454901),
('student', '475', 1461454902),
('student', '476', 1461454903),
('student', '477', 1461454903),
('student', '478', 1461454904),
('student', '479', 1461454905),
('student', '480', 1461454905),
('student', '481', 1461454906),
('student', '482', 1461454907),
('student', '483', 1461454907),
('student', '484', 1461454908),
('student', '485', 1461454909),
('student', '486', 1461454909),
('student', '487', 1461454910),
('student', '488', 1461454911),
('student', '489', 1461454911),
('student', '490', 1461454912),
('student', '491', 1461454913),
('student', '492', 1461454913),
('student', '493', 1461658214),
('student', '494', 1461658214),
('student', '495', 1461658215),
('student', '496', 1461658216),
('student', '497', 1461658216),
('student', '498', 1461658217),
('student', '499', 1461658218),
('student', '500', 1461658218),
('student', '501', 1461658219),
('student', '502', 1461658219),
('student', '503', 1461658220),
('student', '504', 1461658221),
('student', '505', 1461658221),
('student', '506', 1461658222),
('student', '507', 1461658223),
('student', '508', 1461658223),
('student', '509', 1461658224),
('student', '510', 1461658224),
('student', '511', 1461658225),
('student', '512', 1461658226),
('student', '513', 1461658226),
('student', '514', 1461658227),
('student', '515', 1461658228),
('student', '516', 1461658228),
('student', '517', 1461658229),
('sysadmin', '5', NULL),
('sysadmin', '6', NULL),
('teacher', '386', NULL),
('teacher', '387', 1459258422),
('teacher', '388', 1459258448),
('teacher', '393', 1459258451),
('teacher', '394', 1459258452),
('teacher', '395', 1459258453),
('teacher', '396', 1459258453),
('teacher', '397', 1459258454),
('teacher', '398', 1459258455),
('teacher', '399', 1459258455),
('teacher', '400', 1459258456),
('teacher', '401', 1459258457),
('teacher', '402', 1459258457),
('teacher', '403', 1459258458),
('teacher', '404', 1459258458),
('teacher', '405', 1459258459),
('teacher', '409', 1459288297),
('teacher', '410', 1459961090),
('teacher', '411', 1459961211),
('teacher', '414', 1460570140),
('teacher', '415', 1460570803),
('teacher', '416', 1460571217),
('teacher', '417', 1460576542),
('teacher', '420', 1461078480);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dataark for tabell `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, NULL, NULL),
('/admin/*', 2, NULL, NULL, NULL, NULL, NULL),
('/computer/index', 2, 'computer/index', NULL, NULL, NULL, NULL),
('/computer/view', 2, '''/computer/index''', NULL, NULL, NULL, NULL),
('/debug/*', 2, 'debug', NULL, NULL, NULL, NULL),
('/site/*', 2, '2', NULL, NULL, NULL, NULL),
('/site/login', 2, NULL, NULL, NULL, NULL, NULL),
('/site/requestPasswordReset', 2, '1', NULL, NULL, NULL, NULL),
('/student_equipment/my_rent', 2, '/student_equipment/rent', NULL, NULL, NULL, NULL),
('/student_equipment/teachers_request', 2, 'student_equipment/teachers_request', NULL, NULL, NULL, NULL),
('/student_equipment/view', 2, 'student_equipment/view', NULL, NULL, NULL, NULL),
('/student/add_rent_period', 2, '\r\n/teacher_request_rent', NULL, NULL, NULL, NULL),
('/student/changepassword', 2, 'student/changepassword', NULL, NULL, NULL, NULL),
('/student/checkout', 2, '/student/checkout to Teacher', NULL, NULL, NULL, NULL),
('/student/index', 2, 'student overview', NULL, NULL, NULL, NULL),
('/student/mypage', 2, 'min side', NULL, NULL, NULL, NULL),
('/student/update', 2, 'student/update', NULL, NULL, NULL, NULL),
('/student/view', 2, 'view student', NULL, NULL, NULL, NULL),
('accounting_department', 1, 'Accounting department is responsible for student accounting and billing', NULL, NULL, 1459001271, 1459001271),
('change_student_status', 2, 'change_student_status', NULL, NULL, NULL, NULL),
('isStudent', 2, 'is student', NULL, NULL, NULL, NULL),
('rent_equipment', 2, NULL, NULL, NULL, NULL, NULL),
('send_student_list', 2, 'send student name and infor to IKT', NULL, NULL, NULL, 1459001119),
('student', 1, 'can see his own post', NULL, NULL, NULL, NULL),
('sysadmin', 1, 'sysadmin can do anything', NULL, NULL, 1459001375, 1459001401),
('teacher', 1, 'teacher', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dataark for tabell `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('accounting_department', 'change_student_status'),
('accounting_department', 'student'),
('change_student_status', '/student/index'),
('change_student_status', '/student/update'),
('isStudent', '/debug/*'),
('isStudent', '/site/*'),
('isStudent', '/student_equipment/view'),
('isStudent', '/student/changepassword'),
('isStudent', '/student/mypage'),
('isStudent', '/student/update'),
('rent_equipment', '/student_equipment/my_rent'),
('send_student_list', '/student_equipment/teachers_request'),
('send_student_list', '/student_equipment/view'),
('send_student_list', '/student/add_rent_period'),
('send_student_list', '/student/checkout'),
('send_student_list', '/student/view'),
('student', 'isStudent'),
('student', 'rent_equipment'),
('sysadmin', '/*'),
('sysadmin', 'accounting_department'),
('sysadmin', 'student'),
('teacher', '/computer/index'),
('teacher', '/computer/view'),
('teacher', '/site/*'),
('teacher', '/student/view'),
('teacher', 'send_student_list'),
('teacher', 'student');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `borrow_status`
--

CREATE TABLE `borrow_status` (
  `id` int(11) NOT NULL,
  `borrow_status_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `borrow_status_value` smallint(7) NOT NULL,
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `borrow_status`
--

INSERT INTO `borrow_status` (`id`, `borrow_status_name`, `borrow_status_value`, `description`) VALUES
(1, 'teacher_require_rent', 10, 'teacher_require_rent'),
(2, 'has_been_read_by_ikt', 20, 'requirement has been read by ikt'),
(3, 'student_status_validated', 30, ''),
(4, 'worker_try_find_equipment', 40, ''),
(6, 'Utstyr er klart til å hentes', 50, ''),
(7, 'machine_damaged', 60, ''),
(10, 'machine_borrowed', 70, ''),
(11, 'machine_returned', 80, ''),
(12, 'machine_delivered_forever', 90, ''),
(13, 'legitimasjon er fremvist ', 5, 'legitimasjon er fremvist ,kontrakt er motta');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `class_computer`
--

CREATE TABLE `class_computer` (
  `class_id` varchar(20) NOT NULL,
  `class_describe` varchar(50) NOT NULL,
  `allow_computer_type` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `class_computer`
--

INSERT INTO `class_computer` (`class_id`, `class_describe`, `allow_computer_type`) VALUES
('ACH201', 'KunstFag', 20),
('ACH202', 'Dans', 10),
('ACH203', 'Medier', 10),
('ACH204', 'Naturbruk', 10),
('ACH206', 'Idrettsfag', 20),
('ACH209', 'Spill', 10);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `computer`
--

CREATE TABLE `computer` (
  `serial_id` varchar(25) NOT NULL,
  `computer_status` varchar(20) DEFAULT 'STATUS_ACTIVE',
  `model` varchar(20) DEFAULT NULL,
  `computer_name` varchar(20) DEFAULT NULL,
  `shelf_placement` varchar(50) DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `warranty_date` date DEFAULT NULL,
  `end_dato` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `computer`
--

INSERT INTO `computer` (`serial_id`, `computer_status`, `model`, `computer_name`, `shelf_placement`, `purchase_date`, `warranty_date`, `end_dato`) VALUES
('ASDFDFS23GFD', 'STATUS_ACTIVE', 'Samsung 2013-3', 'SAMSUNG WERR12', ' 2.etg-lageret-H6Plass3n', '2016-04-24', '2016-04-27', NULL),
('C02FL9VEDH89', 'STATUS_ACTIVE', 'Mac air 2012', 'Pro Early 2011', ' H6Plass3n', '2016-03-01', '2021-03-01', '2017-03-01'),
('C02FL9VssdGAA', 'STATUS_ACTIVE', 'Dell 2013 kDD', 'Dell 23191 Early 201', ' 2.etg-lageret-H6Plass3n', '2016-03-22', '2016-03-21', '2016-03-22'),
('C0KldksVssdGAA', 'STATUS_ACTIVE', 'Dell 2013 0alwek', 'Pro 231923PRO', ' H6Plass3n', '2016-03-22', '2016-03-21', '2016-03-22'),
('H01F12L9VEDH2G', 'STATUS_ACTIVE', 'Samsung 2013-7', 'Samsung 2013-7', ' H6Plass3n', '2016-04-05', '2016-04-06', '2016-04-04'),
('H01F23L9VEDH2G', 'STATUS_ACTIVE', 'Samsung 2013-6', 'Samsung 2013-6', ' 2.etg-lageret-H6Plass3n', '2016-04-05', '2016-04-06', '2016-04-04'),
('H01F43L9VEDH2G', 'STATUS_ACTIVE', 'Samsung 2013-5', 'Samsung 2013-5', '2.etg-lageret-skap3', '2016-04-05', '2016-04-06', '2016-04-04'),
('H01FL329VPDH2G', 'STATUS_ACTIVE', 'Samsung 2013-4', 'Samsung 2013-4', ' ST3-lageret-H6Plass36', '2016-04-05', '2016-04-06', '2016-04-04'),
('H01FL9VEDH2G', 'STATUS_ACTIVE', 'Samsung 2013-2', 'Samsung 2013-2', ' IKT-kontoret-H6Plass3n', '2016-04-05', '2016-04-06', '2016-04-04'),
('HsdfdL9VssdG', 'STATUS_ACTIVE', 'Mac air 2012', 'Dell 23192 asdf', 'Verkstedet-skap1', '2016-03-22', '2016-03-21', '2016-03-22'),
('SFASDFDDD32', 'STATUS_ACTIVE', 'Mac 01291', 'AIR 23NFV', 'Verkstedet-skap2', '2016-04-26', '2016-04-27', '2016-04-28');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `computer2`
--

CREATE TABLE `computer2` (
  `serial_id` varchar(25) NOT NULL,
  `computer_status` varchar(20) DEFAULT 'STATUS_ACTIVE',
  `type_id` int(2) DEFAULT NULL,
  `model` varchar(20) DEFAULT NULL,
  `computer_name` varchar(20) DEFAULT NULL,
  `cpu` varchar(10) DEFAULT NULL,
  `ram` varchar(10) DEFAULT NULL,
  `storage_capacity` varchar(10) DEFAULT NULL,
  `shelf_placement` varchar(50) DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `warranty_date` date DEFAULT NULL,
  `end_dato` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `computer2`
--

INSERT INTO `computer2` (`serial_id`, `computer_status`, `type_id`, `model`, `computer_name`, `cpu`, `ram`, `storage_capacity`, `shelf_placement`, `purchase_date`, `warranty_date`, `end_dato`) VALUES
('C02FL9VEDH89', 'STATUS_ACTIVE', 20, 'Early 2011', 'Pro Early 2011', 'Core i5', '8GB', '240GB', 'H030_A321', '2016-03-01', '2021-03-01', '2017-03-01'),
('C02FL9VssdGAA', 'STATUS_ACTIVE', 10, 'Dell 2013 kDD', 'Dell 23191 Early 201', 'Intel', '4GB', '300gb', ' H6Plass3n', '2016-03-22', '2016-03-21', '2016-03-22'),
('C0KldksVssdGAA', 'STATUS_ACTIVE', 20, 'Dell 2013 0alwek', 'Pro 231923PRO', 'Intel', '4m', '300gb', ' H6Plass3n', '2016-03-22', '2016-03-21', '2016-03-22'),
('H01F12L9VEDH2G', 'STATUS_ACTIVE', 20, 'Samsung 2013-7', 'Samsung 2013-7', 'Core i7', '16G', '128gb', ' H6Plass3n', '2016-04-05', '2016-04-06', '2016-04-04'),
('H01F23L9VEDH2G', 'STATUS_ACTIVE', 20, 'Samsung 2013-6', 'Samsung 2013-6', 'Core i5', '16G', '128gb', ' H6Plass3n', '2016-04-05', '2016-04-06', '2016-04-04'),
('H01F43L9VEDH2G', 'STATUS_ACTIVE', 20, 'Samsung 2013-5', 'Samsung 2013-5', 'Core i7', '16G', '128gb', ' H6Plass3n', '2016-04-05', '2016-04-06', '2016-04-04'),
('H01FL329VPDH2G', 'STATUS_ACTIVE', 20, 'Samsung 2013-4', 'Samsung 2013-4', 'Core i5', '16G', '300gb', ' H6Plass3n', '2016-04-05', '2016-04-06', '2016-04-04'),
('H01FL9VEDH2G', 'STATUS_ACTIVE', 20, 'Samsung 2013-2', 'Samsung 2013-2', 'Core i5', '4G', '128gb', ' H6Plass3n', '2016-04-05', '2016-04-06', '2016-04-04'),
('HsdfdL9VssdG', 'STATUS_ACTIVE', 20, 'Dell 2013 0alwek', 'Dell 23192 asdf', 'Intel', '4G', '300gb', ' H6Plass3n', '2016-03-22', '2016-03-21', '2016-03-22');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `computer_damage`
--

CREATE TABLE `computer_damage` (
  `id` int(50) NOT NULL,
  `serial_id` varchar(25) NOT NULL,
  `damage_user_id` int(11) NOT NULL,
  `damage_explain` text,
  `image` varchar(100) DEFAULT NULL,
  `registered_at` date NOT NULL,
  `repaired_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `computer_damage`
--

INSERT INTO `computer_damage` (`id`, `serial_id`, `damage_user_id`, `damage_explain`, `image`, `registered_at`, `repaired_date`) VALUES
(12, 'C02FL9VssdGAA', 511, 'hello sir we follow this template 4 all applicatio, i mean fields will be same 4 all or different 4 different application like web based,desktop bases application.hello sir we follow this template 4 all applicatio, i mean fields will be same 4 all or different 4 different application like web based,desktop bases application.hello sir we follow this template 4 all applicatio, i mean fields will be same 4 all or different 4 different application like web based,desktop bases application.hello sir we follow this template 4 all applicatio, i mean fields will be same 4 all or different 4 different application like web based,desktop bases application.hello sir we follow this template 4 all applicatio, i mean fields will be same 4 all or different 4 different application like web based,desktop bases application.', 'Skjermbilde 2016-04-19 00.09.53.png', '1990-01-15', '1999-04-02'),
(18, 'SFASDFDDD32', 388, 'Søkeresultater\r\nphp - Yii2 - yii\\base\\InvalidRouteException - Stack Overflow\r\nstackoverflow.com/.../yii2-yii-base-invalidrouteexce...\r\nOversett denne siden\r\n29. nov. 2014 - You should create a url for that route as follows: <?= Url::toRoute([''site/mostrar-article'', ''id'' => $model->id]) ?> It is explained here ...\r\nphlyty » \\Phlyty\\Exception\\InvalidRouteException\r\nphlyty.readthedocs.org/.../Phlyty.Exception.InvalidR... - Oversett denne siden\r\n', '6.png', '1990-12-20', '1990-12-30'),
(19, 'C02FL9VEDH89', 389, 'asdfAction: mixed, the action (url) used for downloading exported file. Defaults to ''gridview/export/download''.\r\ni18n: array, the internalization configuration for this module. Defaults to:Action: mixed, the action (url) used for downloading exported file. Defaults to ''gridview/export/download''.\r\ni18n: array, the internalization configuration for this module. Defaults to:', '9.png', '0000-00-00', '0000-00-00'),
(22, 'C0KldksVssdGAA', 392, 'kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.cskartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.cskartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.cskartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.cskartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.cskartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.cskartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.cskartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.cskartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.cskartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.cs', '8.png', '2016-04-12', '2016-04-12'),
(23, 'HsdfdL9VssdG', 390, 'sdafsdf', '1.png', '2016-04-24', NULL),
(24, 'H01F23L9VEDH2G', 401, 'sdafsdfaasdfasdf', '7.png', '2016-04-24', NULL),
(25, 'H01FL9VEDH2G', 497, 'dsgfsdfg', '00.png', '2016-04-19', NULL);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `computer_types`
--

CREATE TABLE `computer_types` (
  `id` int(11) NOT NULL,
  `computer_type_name` varchar(30) NOT NULL,
  `computer_type_value` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `computer_types`
--

INSERT INTO `computer_types` (`id`, `computer_type_name`, `computer_type_value`) VALUES
(1, 'PC', 10),
(2, 'Macbook', 20);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1455446495),
('m130524_201442_init', 1455446506),
('m140506_102106_rbac_init', 1458492230);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `model`
--

CREATE TABLE `model` (
  `model_id` int(11) NOT NULL,
  `model_name` varchar(20) DEFAULT NULL,
  `cpu` varchar(10) DEFAULT NULL,
  `ram` varchar(10) DEFAULT NULL,
  `storage_capacity` varchar(10) DEFAULT NULL,
  `computer_type_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `model`
--

INSERT INTO `model` (`model_id`, `model_name`, `cpu`, `ram`, `storage_capacity`, `computer_type_id`) VALUES
(22341233, 'Mac air 2012', '122Hz', '12341234', '14G', 20),
(22341234, 'Mac 01291', '6G', '12', '2G', 20),
(22341235, 'Samsung 2013-2', '4G', '4', '256', 10),
(22341236, 'Samsung 2013-3', '4G', '4', '256', 10),
(22341237, 'Samsung 2013-4', '4G', '4', '256', 10),
(22341238, 'Samsung 2013-5', '4G', '4', '256', 10),
(22341239, 'Samsung 2013-6', '4G', '4', '256', 10),
(22341240, 'Samsung 2013-7', '4G', '4', '256', 10),
(22341241, 'Samsung 2013-8', '4G', '4', '256', 10),
(22341242, ' Dell 2013 kDD', '7G', '6', '256', 10);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `profile`
--

CREATE TABLE `profile` (
  `profile_id` int(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `passive` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(30) NOT NULL,
  `role_value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `role`
--

INSERT INTO `role` (`id`, `role_name`, `role_value`) VALUES
(1, 'student', 10),
(2, 'teacher', 20),
(3, 'accounting_department', 30),
(4, 'admin', 40);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `status`
--

CREATE TABLE `status` (
  `id` smallint(6) NOT NULL,
  `status_name` varchar(30) NOT NULL,
  `status_value` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `status`
--

INSERT INTO `status` (`id`, `status_name`, `status_value`) VALUES
(1, 'ACTIVE', 10),
(2, 'FEE NOT PAID', 20),
(4, 'DAMAGED MACHINE', 30),
(5, 'DELETED', 99);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `student_type`
--

CREATE TABLE `student_type` (
  `id` int(11) NOT NULL,
  `student_type_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `student_type_value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `student_type`
--

INSERT INTO `student_type` (`id`, `student_type_name`, `student_type_value`) VALUES
(1, 'Lower secondary school', 10),
(2, 'High_school', 20);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `system_user`
--

CREATE TABLE `system_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '"here@gmail.com',
  `role_id` smallint(6) NOT NULL DEFAULT '10',
  `user_type_id` smallint(6) DEFAULT '10',
  `status_id` smallint(6) DEFAULT '10',
  `created_at` int(11) DEFAULT NULL,
  `ended_at` int(11) DEFAULT NULL,
  `class_nr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dataark for tabell `system_user`
--

INSERT INTO `system_user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `role_id`, `user_type_id`, `status_id`, `created_at`, `ended_at`, `class_nr`) VALUES
(388, '12343123412', 'laouvj884SYGNrWNHQymSF5zl6I4lb7y', '$2y$12$RyUsqotWmJtHHhoZs7o8wuHYMxgfGJzBVdaGaWuqXj5MbHHmacxOS', NULL, 'wit3@yopmail.com324', 20, 10, 99, 20160428, 20160429, 'ACH203'),
(389, '22009988880', 'hD3hgQaDgO9s864BQJF2vBTm6iMa7YXQ', '$2y$12$RyUsqotWmJtHHhoZs7o8wuHYMxgfGJzBVdaGaWuqXj5MbHHmacxOS', NULL, 'wit3@yopmail.com35', 10, 10, 10, 20160418, 20160427, 'ACH201'),
(390, '22000055222', 'SZ8ARpnKlGEcdm2zqFIqGueis5Ka-BH3', '$2y$12$RyUsqotWmJtHHhoZs7o8wuHYMxgfGJzBVdaGaWuqXj5MbHHmacxOS', NULL, 'wit3@yopmail.com3226', 10, 20, 99, 20160419, 20160430, 'ACH206'),
(391, '23454445454', '54fZwMFOw0hFvAKSIyK55sShI0mORUNk', '$2y$13$Uf36YNnGDO3VQIUxLA0CWOWBgVd0wYobD.oq/rkzjKvRyFQC1hwkm', NULL, 'wit3@yopmail.com37', 20, 10, 10, 20160506, 20160604, 'ACH203'),
(392, '22022222554', 'VaBo53jd0P5VNi6Oo5VSnZNNTSHEgrND', '$2y$13$CDeRLl/MNjZGcJBWvSzIDOfLLROl/k8g8uRgGOvaJPvzkc3aYx30u', NULL, 'wit3@yopmail.com38', 10, 20, 10, 20160425, 20160425, 'ACH203'),
(401, '22011111563', 'aetb4St-K4W718nD95i_b_GqcQ4ceNSG', '$2y$13$gVuTQ7La34iZ.FbXmMeIVuYoS.sUxMzrXeb5pMePTfOh2nAm/vs16', NULL, 'wit3@yopmail.com47', 20, 10, 10, 20160426, 20160430, 'ACH204'),
(402, '22099566699', 'Y3bq1tZV6i_Gip08Rs0qBNDNfAbGgzW7', '$2y$13$fTrL18rxX0q9g5kWLFgn/eZ2w8DZd.2s74YNH/3EFC96PMv80FBii', NULL, 'wit3@yopmail.com31', 20, 10, 10, 20160419, 20160427, 'ACH203'),
(408, '12345671234', '4vZ1w78NMMxXnQcWaOf5M8UpvuhIO1Zc', '$2y$12$RyUsqotWmJtHHhoZs7o8wuHYMxgfGJzBVdaGaWuqXj5MbHHmacxOS', NULL, 'v22iasdf@jod.nssssss', 10, 20, 10, 20160426, 20160506, 'ACH202'),
(409, '12341234123', 'st6lPUQbEy_qgj3ljdbva8zGF-dTdANu', '$2y$13$BnNDRW6KJaRj1j/pijRDG.trTH4.wQB3Aap3tmJuvhn8hqrDaS2am', NULL, 'v22iasdf@jod.nssss', 20, 10, 10, 20160428, 20160428, 'ACH202'),
(410, '12342134344', 'vypFYkCCxi4I9gt7_kexTghEdQQPlfaK', '$2y$13$HD6ls9hvwN6FHoaY8qkB5u48wigwKp2FRl1MbTbN2Al2kbOB1F.LO', NULL, 'v22iasdf@jod.nsssssssss', 20, 10, 10, 20160502, 20160604, 'ACH202'),
(422, '12376789098', NULL, NULL, NULL, '222hereeee@geemail.com', 10, 10, 10, 20160425, 20160429, 'ACH204'),
(493, '90550123233', NULL, '$2y$13$A.J1fJe7anb1S/88.EesDOpNhIM2/YyFbZgrGy2fBzV5CP.Ew8zeO', NULL, 'wit3@yopmail.com34129', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(494, '99055112345', NULL, '$2y$13$CLYHYQkn7bNrZiR1En1SoOZH83UsaDEMMftGjfMAhPpFueUBXTRqi', NULL, 'wit3@yopmail.com34130', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(495, '92055212121', NULL, '$2y$13$YpdKVFzrqaaXzqwh136O7uiggaynGEEDyzzt.I2und59fRvuzpGKi', NULL, 'wit3@yopmail.com34131', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(496, '92055212122', NULL, '$2y$13$w74whGKptFibZartxB93vuBr.dCy1I749tQOnkUU4LJ1Y1fjheOEO', NULL, 'daniel1504@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(497, '92055212123', NULL, '$2y$13$MkBbO2OkgeskjnIQbNZvsez7pW6ioMvVWdCQKhSjjMlOi3b0QHNMe', NULL, 'rehan2412@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(498, '92055212124', NULL, '$2y$13$fa9cXu7lMjd6yrheHAC7VOce1PDIkjBfKihBzg98nJ30iV9JHn53a', NULL, 'sinem2010@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(499, '92055212125', NULL, '$2y$13$nmXTdrNa73jYUM22O8oADerd24YtNDPR5Yf22ox6qsbnWCH8s97Xi', NULL, 'byll2909@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(500, '92055212126', NULL, '$2y$13$QHlqD7lntlVYrrnSXR3T7.jRChZyVSMcI140Lom0cG1PP9qoi5bdC', NULL, 'abcia003@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(501, '92055212127', NULL, '$2y$13$w3I1QltLyOHz/BG9fDikPeV7M.RYd/qhMXxF/z7LArV0zWoXllZSu', NULL, 'fatih1508@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(502, '92055212128', NULL, '$2y$13$LzNk6wakJAZ5G5qtvJG/zOX5Z8NXLPdcrJZXWQugsUTPq1mSImBvi', NULL, 'duy0508@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(503, '92055212129', NULL, '$2y$13$r3D89MwlZJiaQn9FpfbkjOM7AcE6BRbkZTrCkCR/Jk2KrX4U4dedu', NULL, 'jenoth2209@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(504, '92055212130', NULL, '$2y$13$atxgHIY6Rdvx1dO3aZmHuOQfz.KmxTrDmKX14SNHzp9B3CaA7KjmS', NULL, 'selina1503@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(505, '92055212131', NULL, '$2y$13$.GDWn0BKVN1VJFUOoFLPn.iCfvXajNfCZTBtJdF2ZVGOYBBKYt75m', NULL, 'ibtiss0507@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(506, '92055212132', NULL, '$2y$13$VFPACvvUiOgwVDdzFQJZC.WR3fQe6RPEM3aCpqf9YWtMEPmr7ixRu', NULL, 'sabrin0709@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(507, '92055212133', NULL, '$2y$13$YpHM5yzDy8xsDYyiT/2Vm.5KcB9LyDfqbzDzoeNovtKM5TpmNisZe', NULL, 'daniel15d04@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(508, '92055212134', NULL, '$2y$13$MPhbvL6DG/fVmHiYNnSEruz4TkOWSJhh1GVuESlF68h11CivL6mV2', NULL, 'rehan2f412@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(509, '92055212135', NULL, '$2y$13$D8AG6kbe0GbRb4IiX.Du.O59zjOO7bY6kyKFE5/jTJtunKecxfcEu', NULL, 'sinem2g010@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(510, '92055212136', NULL, '$2y$13$jdtjTe3pNDuulqDkpzL6tecUc8kCygZEZ.cI/Cu/.n5caulPmqaKS', NULL, 'byll29f09@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH201'),
(511, '92055212137', NULL, '$2y$13$Q6OoqslxhY3i5Kdxi7oW9u3J0quBm/r5uopfTw/B./5o5.iULLqmG', NULL, 'abcia00g3@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH205'),
(512, '92055212138', NULL, '$2y$13$b7eRyQwuQZJEdm.QOe1SD.41G8fN86G0l5RkctfmgR2Rzm6M5CIrq', NULL, 'fatih1g508@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH206'),
(513, '92055212139', NULL, '$2y$13$Yf/Yv/7zzZDU6Ubt8IcdV.zHkYA03o3WQGdETgVKBJbEF3exoGE9W', NULL, 'duy05g08@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH207'),
(514, '92055212140', NULL, '$2y$13$gfVPM4VFwZhm26fkSGo9UemHA0RwDHs/5krWc8zGmLASTZyzvi0Im', NULL, 'jegnoth2209@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH208'),
(515, '92055212141', NULL, '$2y$13$uEH5DFtJ7jKHaxoH4OJ80e6j4sTQrr39bCreSjUAQBMTeV6LzZ7B.', NULL, 'selina1503g@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH209'),
(516, '92055212142', NULL, '$2y$13$iGMrMZj5ELfjuwUpqRdatuB4xWhxzbHYy9EYqfJhYhnRWBzLgQbGW', NULL, 'ibtiss0507g@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH210'),
(517, '92055212143', NULL, '$2y$13$S/sVUHwuF9anwtw.KyRjhevTFSEW4jLESledmp3BQjDXL5SQ.GbTu', NULL, 'assabrin0709@osloskolen.no', 10, 10, 10, 20160203, 20160211, 'ACH211'),
(518, '12341234456', NULL, NULL, NULL, 'vickssssy@gmaisd.cn', 20, 10, 10, 20160504, 20160518, 'ACH203'),
(519, '12123412341', NULL, NULL, NULL, 'v22iasdf@jod.nssssss5', 30, 10, 10, 20160426, 20160506, 'ACH203'),
(520, '12383834928', NULL, NULL, NULL, 'v22iasu9ddf@jod.nssssss5', 30, 10, 10, 20160502, 20160502, ''),
(521, '12341234324', NULL, NULL, NULL, 'v111122iasdf@jod.nssssss5', 10, 10, 10, 20160328, 20160328, 'ACH204');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `user_equipment`
--

CREATE TABLE `user_equipment` (
  `id` int(11) NOT NULL,
  `renter_id` int(11) NOT NULL,
  `serial_id` varchar(25) DEFAULT NULL,
  `borrow_status_id` int(11) NOT NULL,
  `require_by_teacher_id` int(11) NOT NULL,
  `start_at` date NOT NULL,
  `end_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `user_equipment`
--

INSERT INTO `user_equipment` (`id`, `renter_id`, `serial_id`, `borrow_status_id`, `require_by_teacher_id`, `start_at`, `end_at`) VALUES
(45, 392, 'C0KldksVssdGAA', 50, 409, '1900-12-28', '1900-12-29'),
(65, 402, 'H01F12L9VEDH2G', 60, 389, '0000-00-00', '0000-00-00'),
(68, 390, 'C02FL9VEDH89', 70, 401, '0000-00-00', '0000-00-00'),
(70, 410, 'H01F23L9VEDH2G', 50, 392, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `user_invoice`
--

CREATE TABLE `user_invoice` (
  `id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `invoice_url` varchar(25) DEFAULT NULL,
  `accounting_id` int(11) NOT NULL,
  `payment_deadline_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `user_invoice`
--

INSERT INTO `user_invoice` (`id`, `receiver_id`, `invoice_url`, `accounting_id`, `payment_deadline_at`) VALUES
(45, 392, 'C0KldksVssdGAA', 409, '0000-00-00'),
(53, 409, '12341234', 409, '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_user`
--
ALTER TABLE `admin_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `borrow_status`
--
ALTER TABLE `borrow_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_computer`
--
ALTER TABLE `class_computer`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `computer`
--
ALTER TABLE `computer`
  ADD PRIMARY KEY (`serial_id`);

--
-- Indexes for table `computer2`
--
ALTER TABLE `computer2`
  ADD PRIMARY KEY (`serial_id`);

--
-- Indexes for table `computer_damage`
--
ALTER TABLE `computer_damage`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `serial_id` (`serial_id`),
  ADD UNIQUE KEY `damage_user_id` (`damage_user_id`);

--
-- Indexes for table `computer_types`
--
ALTER TABLE `computer_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `computer_type_value` (`computer_type_value`),
  ADD KEY `computer_type_value_2` (`computer_type_value`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `model`
--
ALTER TABLE `model`
  ADD PRIMARY KEY (`model_id`),
  ADD UNIQUE KEY `model_name` (`model_name`),
  ADD KEY `type` (`computer_type_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`profile_id`,`user_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_type`
--
ALTER TABLE `student_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_user`
--
ALTER TABLE `system_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `user_equipment`
--
ALTER TABLE `user_equipment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `student_id_2` (`renter_id`),
  ADD UNIQUE KEY `serial_id` (`serial_id`),
  ADD KEY `student_id` (`renter_id`);

--
-- Indexes for table `user_invoice`
--
ALTER TABLE `user_invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `student_id_2` (`receiver_id`),
  ADD UNIQUE KEY `serial_id` (`invoice_url`),
  ADD KEY `student_id` (`receiver_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_user`
--
ALTER TABLE `admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `borrow_status`
--
ALTER TABLE `borrow_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `computer_damage`
--
ALTER TABLE `computer_damage`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `computer_types`
--
ALTER TABLE `computer_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `model`
--
ALTER TABLE `model`
  MODIFY `model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22341243;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `profile_id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `student_type`
--
ALTER TABLE `student_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `system_user`
--
ALTER TABLE `system_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=522;
--
-- AUTO_INCREMENT for table `user_equipment`
--
ALTER TABLE `user_equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `user_invoice`
--
ALTER TABLE `user_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Begrensninger for tabell `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `computer_damage`
--
ALTER TABLE `computer_damage`
  ADD CONSTRAINT `computer_damage_ibfk_1` FOREIGN KEY (`serial_id`) REFERENCES `computer` (`serial_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `computer_damage_ibfk_2` FOREIGN KEY (`damage_user_id`) REFERENCES `system_user` (`id`) ON UPDATE CASCADE;

--
-- Begrensninger for tabell `user_equipment`
--
ALTER TABLE `user_equipment`
  ADD CONSTRAINT `user_equipment_ibfk_1` FOREIGN KEY (`renter_id`) REFERENCES `system_user` (`id`);

--
-- Begrensninger for tabell `user_invoice`
--
ALTER TABLE `user_invoice`
  ADD CONSTRAINT `user_invoice_ibfk_1` FOREIGN KEY (`receiver_id`) REFERENCES `system_user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
