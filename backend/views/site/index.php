<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\icons\Icon;
use yii\bootstrap\Modal;
Icon::map($this,Icon::FA);


$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = ['label' => Icon::show('laptop', [], Icon::FA).'Dashboard', 'url' => ['index'],'encode' => false,];




?>
<div class="site-index wrapper">
    <div class="jumbotron"><h1>
        <?php echo Yii::t('app', 'Congratulations!'); ?></h1>
        <p class="lead"><?php echo Yii::t('app', 'You have been successfully log in as admin.'); ?></p>
        <div class="jumbotron">
<?php echo Icon::show('user', ['class'=>'fa-4x icon-stack-1x'], Icon::FI); ?>
       
            </div>
       
    </div>
</div>
