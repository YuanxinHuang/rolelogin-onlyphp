<?php 
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Student */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Student list', 'url' => ['checkout']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="student-view wrapper">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
             [
                'label' => 'Personal number',
                'attribute'=>'username',
                'value'=>$model->username,
             ],
            'email:email',
            'role_id',
            'status_id',
            [
                'label' => 'This person started school at:',
                'attribute'=>'created_at:datetime',
                'value'=>$model->created_at,
                'format' =>'datetime',
             ],
            [
                'label' => 'This person ended school at:',
                'attribute'=>'ended_at:datetime',
                'value'=>$model->ended_at,
//                'format' =>'datetime',
             ],
            'class_nr' 
        ],
    ]) ?>

<!--</div>-->
