<!--Admin login-->



<?php //

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\icons\Icon;
Icon::map($this,Icon::FA);

$this->title = 'Admin Logg inn';
//$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
    <div class="desktop-only">
     
     <svg class="mh6 w vat font-effect-shadow-multiple" viewBox="0 0 315 28">
        <text class="w lsxs  ttu" x="35" y="20"><tspan class="fillMain">DATA REGISTER&#8208</tspan>
            <tspan class="fillDescript">BJØRNHOLT SKOLE</tspan>
        </text>
      </svg>
     
    </div>

    
<div class="row login_image">  
    <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 box-shadow login-wrapper">
       
    <div>
            <div class="image-logo hidden-sm hidden-xs">
                <span class="icon-stack">
          <?php 
         echo Icon::show('user', ['class'=>'fa-4x icon-stack-1x'], Icon::FA);
//       echo Icon::showStack('circle', 'user', ['class'=>'fa-lg'], ['class'=>'fa-inverse'],Icon::FA);
         echo Icon::show('wrench', ['class'=>'fa-2x icon-stack-2x'], Icon::FA);
          ?>
                </span>
            </div>
    </div>

<div class="site-login ">
    <div class="info"><h2><span class="icon-stack hidden-md hidden-lg hidden-xl">
          <?php 
//       echo Icon::showStack('circle', 'user', ['class'=>'fa-lg'], ['class'=>'fa-inverse'],Icon::FA);
         echo Icon::show('wrench', ['class'=>'fa-sm'], Icon::FA);
          ?>
              </span><?= Html::encode($this->title) ?></h2><p>
        <?php echo Yii::t('app', 'Please fill this form for login:').'</p><p>Test med brukernavn-admin og passord-asdf1234</p>'; ?>
                </div>

    <div>
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?=
                    $form->field($model, 'username')
                    ->label(Yii::t('app', 'Norwegian Person Number'))
                ?>
                <?=
                    $form->field($model, 'password')
                    ->label(Yii::t('app', 'Password'))
                    ->passwordInput()
                ?>
                <div class="pull-right language bg-success col-xs-12 col-md-4">
<?= Html::submitButton(Yii::t('app','Log in'), ['class' => 'btn col-xs-12 bg-success',
 'name' => 'login-button']) ?>
                   
                </div> 
          <?php echo  '<a href="'.Yii::$app->request->baseUrl.'/admin.php/site/request_password_reset">'.
                  Yii::t('app', 'Forget your password?'); ?>
             </a>
                    
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>