<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ComputerDamageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="computer-damage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'serial_id') ?>

    <?= $form->field($model, 'damage_user_id') ?>

    <?= $form->field($model, 'damage_explain') ?>

    <?= $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'registered_at') ?>

    <?php // echo $form->field($model, 'repaired_date') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
