<?php

use kartik\icons\Icon;
use yii\bootstrap\Modal;
Icon::map($this,Icon::FA);

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = ['label' => Icon::show('laptop', [], Icon::FA).'Dashboard', 'url' => ['index'],'encode' => false,];

?>
<div class="body-content-index">

        <div class="row">
         
            <div class="col-lg-12">
                <h2>Hva kan jeg hjelpe deg med?</h2>

        <div class="third">
          <div id="event">
            <a id="ca" href="ca.html"><img src="img/map-ca.png" alt="San Francisco" />San Francisco, CA</a>
            <a id="tx" href="tx.html"><img src="img/map-tx.png" alt="Austin, TX" />Austin, TX</a>
            <a id="ny" href="ny.html"><img src="img/map-ny.png" alt="New York, NY" />New York, NY</a>
          </div>
        </div>
        <div class="third">
          <div id="sessions">
            <p>Select an event from the left</p>
          </div>
        </div>
        <div class="third">
          <div id="details"></div>
        </div>


    </div>
</div>

