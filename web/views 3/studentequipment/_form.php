<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

//use common\models\Computer;
use kartik\select2\Select2;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model backend\models\StudentEquipment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-equipment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'renter_id')->widget(Select2::classname(), [
    'data' => $model->getRenterList(),
    'language' => 'no',
    'options' => ['placeholder' => 'Select a person  ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ]) ?>

    <?= $form->field($model, 'serial_id')->widget(Select2::classname(), [
    'data' => $model->getEquipmentList(),
    'language' => 'no',
    'options' => ['placeholder' => 'Select a computer  ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ]) ?>

    <?= $form->field($model, 'borrow_status_id')->widget(Select2::classname(), [
    'data' => $model->getStatusList(),
    'language' => 'no',
    'options' => ['placeholder' => 'Select a status name..'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ]) ?>

    <?= $form->field($model, 'require_by_teacher_id')->widget(Select2::classname(), [
    'data' => $model->getRenterList(),
    'language' => 'no',
    'options' => ['placeholder' => 'Select a person  ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ]) ?>

    <?= $form->field($model, 'start_at')->widget(DatePicker::className(),[
    'name' => 'start_at', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>
    
     <?= $form->field($model, 'end_at')->widget(DatePicker::className(),[
    'name' => 'end_at', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
