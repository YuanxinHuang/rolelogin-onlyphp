<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "system_user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role_id
 * @property integer $user_type_id
 * @property integer $status_id
 * @property integer $created_at
 * @property integer $ended_at
 * @property string $class_nr
 *
 * @property ComputerDamage $computerDamage
 * @property UserEquipment $userEquipment
 */
class SystemUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['role_id', 'user_type_id', 'status_id', 'created_at', 'ended_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['class_nr'], 'string', 'max' => 20],
            [['username'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'role_id' => Yii::t('app', 'Role ID'),
            'user_type_id' => Yii::t('app', 'User Type ID'),
            'status_id' => Yii::t('app', 'Status ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'ended_at' => Yii::t('app', 'Ended At'),
            'class_nr' => Yii::t('app', 'Class Nr'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComputerDamage()
    {
        return $this->hasOne(ComputerDamage::className(), ['damage_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserEquipment()
    {
        return $this->hasOne(UserEquipment::className(), ['renter_id' => 'id']);
    }
}
