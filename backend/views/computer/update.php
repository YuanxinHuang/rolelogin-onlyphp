<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
use yii\helpers\ArrayHelper;
use common\models\Computer;
use common\models\Student;
use kartik\select2\Select2;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\Computer */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Computer',
]) . $model->serial_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Computers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->serial_id, 'url' => ['view', 'id' => $model->serial_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="computer-update wrapper">
       <?= Html::a(Yii::t('app', 'Back'), ['index'], [
            'class' => 'btn btn-default pull-right',
        ]) ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="computer-form">

    <?php error_reporting(E_ALL); 
    $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['deviceSize' => ActiveForm::SIZE_SMALL],
]); ?>

    <?= $form->field($model, 'serial_id')->textInput(['readonly' => true]) ?> 
    <?= $form->field($model, 'model')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(common\models\Laptopmodel::find()->all(),'model_name','model_name'),
    'language' => 'no',
    'options' => ['placeholder' => 'Select a model type ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ]) ?>

    <?= $form->field($model, 'computer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shelf_placement')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(common\models\Computer::find()->all(),'shelf_placement','shelf_placement'),
    'size' => Select2::MEDIUM,
    'options' => ['placeholder' => 'Select a shelf placement ...'],
    'pluginOptions' => [
        'hideSearch'=> true
          ],
    ]) ?>
    <?= $form->field($model, 'purchase_date')->widget(DatePicker::className(),[
    'name' => 'purchase_date', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>

    <?= $form->field($model, 'warranty_date')->widget(DatePicker::className(),[
    'name' => 'warranty_date', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>
    
    <?= $form->field($model, 'end_dato')->widget(DatePicker::className(),[
    'name' => 'end_dato', 
    'value' => date('d-M-Y', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]) ?>
        <!-- Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])--> 
    <?= Html::submitButton('submit', ['class' => 'btn btn-success']) ?>

    <?php ActiveForm::end(); ?>

</div>

</div>
