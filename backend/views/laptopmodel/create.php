<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Laptopmodel */

$this->title = Yii::t('app', 'Create Laptopmodel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laptopmodels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laptopmodel-create wrapper">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
