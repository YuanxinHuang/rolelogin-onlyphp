<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "computer_type".
 *
 * @property integer $id
 * @property string $computer_type_name
 * @property integer $computer_type_value
 */
class Computer_type extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'computer_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['computer_type_name', 'computer_type_value'], 'required'],
            [['computer_type_value'], 'integer'],
            [['computer_type_name'], 'string', 'max' => 30],
            [['computer_type_value'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'computer_type_name' => Yii::t('app', 'Computer Type Name'),
            'computer_type_value' => Yii::t('app', 'Computer Type Value'),
        ];
    }
    
    public function getComputers()
    {
        return $this->hasMany(Computer::className(), ['model' => 'model_name'])
                    ->viaTable('model', ['computer_type_id' => 'computer_type_value']);
    }
    
     public function getComputerTypeName(){
         return $this->computer_type_name;
     }
}
