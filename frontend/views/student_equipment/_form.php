<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\StudentEquipment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-equipment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'renter_id')->textInput() ?>

    <?= $form->field($model, 'serial_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'borrow_status_id')->textInput() ?>

    <?= $form->field($model, 'require_by_teacher_id')->textInput() ?>

    <?= $form->field($model, 'start_at')->textInput() ?>

    <?= $form->field($model, 'end_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
