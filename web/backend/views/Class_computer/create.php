<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Class_computer */

$this->title = Yii::t('app', 'Create Class Computer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Class Computers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="class-computer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
