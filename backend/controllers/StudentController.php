<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\base\ErrorException;
use backend\models\UploadForm;
use backend\models\ChangePasswordForm;
use backend\models\StudentCreate;
use backend\models\StudentUpload;
use common\models\Student;
use common\models\StudentSearch;
use common\models\StudentLoan;
/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends BackendBaseController
{
    public function behaviors()
    {
        return [
      
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
                    'access' => [
                        'class' => AccessControl::className(),
                        'only' => ['index','create','update','view','checkout','upload','delete'],
                        'rules' => [
                            [
                                'actions' => ['view', 'index','create','checkout','upload','uploadform','update','delete','passwordlist','newuser','setnewpassword'],
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                        ],
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new StudentCreate();
        if (Yii::$app->request->isPost) {
             if ($model->signup()) {
                Yii::$app->session->setFlash('success', 'You had added an user.');
               return $this->render('create', ['model' => $model]);
            } else {
                throw new NotFoundHttpException('You had not added an use.');
            }
            } else {
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);}
    }
    
        /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionPasswordlist()
    {
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new StudentCreate();
        if (Yii::$app->request->isPost) {
             if ($model->signup()) {
                Yii::$app->session->setFlash('success', 'You had added an user.');
               return $this->render('create', ['model' => $model]);
            } else {
                throw new NotFoundHttpException('You had not added an use.');
            }
            } else {
        return $this->render('passwordList', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);}
    }

    /**
     * Displays a single Student model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Student();
            if ($model->load(Yii::$app->request->post())) {
                    if ($model->save()){
                          Yii::$app->session->setFlash('success', Yii::t('app', 'You had added a new user.'));
                           return $this->redirect(['index']);     
            } else {
                Yii::$app->session->setFlash('error', 'You had not added any user.');
                throw new NotFoundHttpException('You had not added an user.');
            }
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                        'model' => $model
            ]);
        }
        else return $this->render('create', ['model' => $model]);
    }
    
      /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionUploadform()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
        
            $inFile = $model->file = UploadedFile::getInstance($model, 'file');
            $size = 2 * 1024 * 100;
            if (!$inFile->size || $inFile->size > $size) {
                \Yii::$app->getSession()->setFlash('error', 'File canot be bigger than 200kb.');
                return $this->render('upload', ['model' => $model]);
            }
            try {
                $model->upload();
                // file is uploaded successfully
                Yii::$app->session->setFlash('success', 'You had added a file.');
            } catch (NotFoundHttpException $e) {
                return $this->render('error', ['exception' => $e->getMessage()]);
            }
            return $this->redirect(['upload']);
        } else {
            return $this->render('upload', ['model' => $model]);
        }
    }
    
     /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

     public function actionUpload()
    {
        $model = new StudentUpload();

        if (Yii::$app->request->isPost) {
                    if ($data = $model->insertData()) {
                    // data is uploaded successfully to db 
                    Yii::$app->session->setFlash('success', ' Greate! You had added USERS.');
//                    return $this->redirect(['index']); 
                    } else {     
                    Yii::$app->session->setFlash('success', ' Sorry! You had not added any USERS.');
                    }
     }
        return $this->render('readUpload', [
                'model' => $model,
    ]);
    }
     /**
     * this excel upload view will appear in Bootstrap Modal
     * If update is post, it will redirect to the upload page.
     * @param integer $id
     * @return mixed
     */
        public function actionAddexcel()
    {
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new UploadForm();
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            try {
                $model->upload();
                // file is uploaded successfully
                Yii::$app->session->setFlash('success', 'You had added a file.');
                return $this->redirect(['upload']);
            } catch (NotFoundHttpException $e) {
                return $this->render('error', ['exception' => $e->getMessage()]);
            }
            } else {
        return $this->render('addExcel', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);}
    }
    
     /**
     * checkoutbox an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
        public function actionCheckout()
    {
        $searchModel = new StudentSearch();
        $dateModel = new StudentLoan();
        if(isset($_POST['keylist']))
        {
            $keylist = $_POST['keylist'];
            $data = $searchModel->getStudentsByIds($keylist);
            return $this->render("confirm_check", array('keylist' => $keylist, 'data'=>$data, 'model'=>$dateModel));
        } else if(isset($_POST['idlist']))
        {
            $idlist = $_POST['idlist'];
            $dateModel->load(Yii::$app->request->post());
            $dateModel->loanEquipment($idlist);
        } else if(isset($_POST['sample_1_length']))
        {
            $data = $searchModel->getAllDataByRoleType();
            return $this->render("students_check",array( 'data'=>$data, 'flag'=>'1'));
        }
        $data = $searchModel->getAllDataByRoleType();
        return $this->render("students_check",array( 'data'=>$data));
    }
    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'New changes are registed in database');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error',  'You havn´t done any changes yet');
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        } else { 
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
     public function actionResetpassword(){
        $id=Yii::$app->user->id;
        $_user = $this->findModel($id);
        $model= new ChangePasswordForm();
            if ($model->load(Yii::$app->request->post())) {        
                if($model->setnewPassword($_user)){
                Yii::$app->session->setFlash('success',  'You had changed your password');
                return $this->redirect(['mypage']);
           
                }
            } else {
            return $this->render('changepassword', [
                'model' => $model,
                ]);
            }
    }
        public function actionSetnewpassword($id){
        $model = $this->findModel($id);
        $modelForm= new ChangePasswordForm();
              if ($modelForm->load(Yii::$app->request->post())) {
                if ($modelForm->setnewPassword($model)){
                Yii::$app->session->setFlash('success', 'New changes are registed in database');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
//                Yii::$app->session->setFlash('error',  'You havn´t done any changes yet');
                return $this->render('setnewpassword', [
                            'model' => $modelForm,
                ]);
            }
        } else { 
            return $this->render('setnewpassword', [
                'model' => $modelForm,
            ]);
        }
    }
    /**
     * Deactivate an existing Student model.
     * If deactivate is successful, the browser will be redirected to the 'index' page and return a alert.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $student = $this->findModel($id);
        $student->softDelete();
        if($student->save()){
            Yii::$app->session->setFlash('success',  'You had deleted a user');
          return $this->redirect(['index']);  
        }
        
    }
    
    public function actionRecover($id)
    {
        $student = $this->findModel($id);
        $student->recreate();
        if($student->save()){
            Yii::$app->session->setFlash('success',  'You had recover a user');
          return $this->redirect(['index']);  
        }
        
    }
    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Send debug code to the Javascript console
     * @param integer $data
    */ 
    public function debug_to_console($data) {
    if(is_array($data) || is_object($data))
	{
		return json_encode($data);
	} else {
		return "<script>console.log('PHP: ".$data."');</script>";
	}
}
}
