<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ComputerType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="computer-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'computer_type_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'computer_type_value')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
