<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Computer;
use common\models\Student;
use kartik\select2\Select2;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\ComputerDamage */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Computer Damage',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Computer Damages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="computer-damage-update wrapper">
    <?= Html::a(Yii::t('app', 'Back'), ['index'], [
            'class' => 'btn btn-default pull-right',
        ]) ?>
       <?php
     $attributes = [
        [
        'group'=>true,
        'label'=>'SECTION 1: Damage user Information',
        'rowOptions'=>['class'=>'info']
        ],  
        [
        'columns' => [
                [
                    'attribute'=>'id', 
                    'format'=>'raw', 
                    'value'=>'<kbd>'.$model->id.'</kbd>',
                    'valueColOptions'=>['style'=>'width:10%'], 
                    'displayOnly'=>true,
                ],
                [
                    'attribute'=>'serial_id', 
                    'value'=> $model->serial_id,
                    'type'=>DetailView::INPUT_SELECT2,
                        'widgetOptions'=>[
                        'data'=>ArrayHelper::map(Computer::find()->orderBy('computer_name')->asArray()->all(), 'serial_id','computer_name'),
                        'options' => ['placeholder' => 'Select...'],
                        'pluginOptions' => ['allowClear'=>true, 'width'=>'100%'],
                        ],
                ],
                [
                    'attribute'=>'damage_user_id',
                    'value'=>$model->damage_user_id,
                    'type'=>DetailView::INPUT_SELECT2,
                    'widgetOptions'=>[
                    'data' => ArrayHelper::map(Student::find()->all(),'id','username'),
                    'options' => ['placeholder' => 'Select a student..'],
                     'pluginOptions' => ['allowClear' => true],
                    ],
                ], 
            ],
        ],  
     [
        'columns' => [
            [
                'attribute'=>'damage_explain', 
                'value'=> $model->damage_explain,
                'type'=>DetailView::INPUT_TEXTAREA, 
                'options'=>['rows'=>10]
            ],
        ],
    ],          
    
        [
        'group'=>true,
        'label'=>'SECTION 2: Damaged Date and Repaired Date',
        'rowOptions'=>['class'=>'info']
        ],
        [
            'columns' => [
            [
             
                'attribute'=>'registered_at', 
                'label'=>'Registered date',
                'type'=>DetailView::INPUT_DATE,
                'widgetOptions'=>[
                'name' => 'registered_at', 
                    'options' => ['placeholder' => 'Select date ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ],
                ],
                'valueColOptions'=>['style'=>'width:30%']
             ], 
             [
             
                'attribute'=>'repaired_date', 
                'label'=>'Repaired_date',
                'type'=>DetailView::INPUT_DATE,
                'widgetOptions'=>[
                'name' => 'registered_at', 
                    'options' => ['placeholder' => 'Select date ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ],
                ],
             ],             
            ],
        ],
    ];
    
    echo DetailView::widget([
    'model' => $model,
    'attributes' => $attributes,
    'condensed'=>true,
    'hover'=>true,
    'mode' => DetailView::MODE_EDIT,
    'fadeDelay'=>600,
    'panel' => [
        'heading'=>'<img src="'.Yii::$app->request->BaseUrl.'/assets/images/kommuneimage.png" alt="service invoice" style="height:128px;">',
        'type'=>DetailView::TYPE_DEFAULT,
        ],
    'deleteOptions' => [ // your ajax delete parameters
        'params' => ['id' => $model->id, 'kvdelete' => true],
    ],
     'container' => ['id'=>'kv-demo'],
     'formOptions' => ['action' => Url::current(['#' => 'kv-demo'])] // your action to delete
    ]) ?>

</div>
