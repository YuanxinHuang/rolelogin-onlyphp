<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-form ">

    <?php error_reporting(E_ALL); 
    $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['deviceSize' => ActiveForm::SIZE_SMALL],
]); ?>
    <?= $form->field($model, 'username',['labelOptions'=>['class'=>'col-sm-2 col-md-2']])->textInput(['maxlength' => true])
            ->hint(Yii::t('app', 'Please enter the national number'))->label(Yii::t('app', 'National number'))  ?>
    
    <?= $form->field($model, 'email',['labelOptions'=>['class'=>'col-sm-2 col-md-2']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role_id')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(\backend\models\Role::find()->all(),'role_value','role_name'),
    'options' => ['placeholder' => 'Select a role type ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ])->label(Yii::t('app', 'User is')) ?>
   
    <?= $form->field($model, 'class_nr')->widget(Select2::classname(), [
    'data' => $model->getClassList(),
    'options' => ['placeholder' => 'Select a Class name ...'],
    'pluginOptions' => [
        'allowClear' => true
          ],
    ])->label('Class') ?>
    <?php 
    echo '<label class="control-label">Valid Dates</label>';
    echo DatePicker::widget([
    'model' => $model,
    'attribute' => Yii::t('app', 'created_at'),
    'attribute2' => Yii::t('app', 'ended_at'),
    'options' => ['placeholder' => 'Start date of school'],
    'options2' => ['placeholder' => 'End date of school'],
    'type' => DatePicker::TYPE_RANGE,
    'form' => $form,
    'pluginOptions' => [
        'format' => 'yyyymmdd',
        'autoclose' => true,
    ]
]);?>

<div>
    <hr>
    <?= Html::submitButton(Yii::t('app', 'submit'), ['class' => 'btn btn-success']) ?>
    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
</div>
    <?php ActiveForm::end(); ?>

</div>
