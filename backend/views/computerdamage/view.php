<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Computer;
use common\models\Student;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\ComputerDamage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Computer Damages list'), 'url' => ['index']];
?>
<div class="computer-damage-view wrapper">
    <?= Html::a(Yii::t('app', 'Back'), ['index'], [
    'class' => 'btn btn-default pull-right',
    'target'=>'_blank', 
    'data-toggle'=>'tooltip', 
    'title'=>'Will come back to the list page'
    ]) ?>
   <?= Html::a('Download Pdf', ['/computerdamage/downloadpdf/?id='.$model->id], [
    'class'=>'btn btn-danger pull-right downloadbutton', 
    'target'=>'_blank', 
    'data-toggle'=>'tooltip', 
    'title'=>'Will open the generated PDF file in a new window'
]);
     $attributes = [
        [
        'group'=>true,
        'label'=>'SECTION 1: Damage and User Information',
        'rowOptions'=>['class'=>'info']
        ],
        [
        'columns' => [

            [
                'attribute'=>'image',
                'value'=>$model->imageurl,
                'format' => ['image',['width' => '100%', 'display'=>'block','height'=> 'auto','alt'=>"damage computer:.$model->serial_id."]],
                'type'=>DetailView::INPUT_FILE,
                'widgetOptions'=>[
                        'pluginOptions' => [
                            'width'=>'100%',
                            ],
                      ],
                ],  
        ],
    ],   
        [
        'columns' => [
                [
                    'attribute'=>'id', 
                    'format'=>'raw', 
                    'value'=>'<kbd>'.$model->id.'</kbd>',
                    'valueColOptions'=>['style'=>'width:10%'], 
                    'displayOnly'=>true,
                ],
                [
                    'attribute'=>'serial_id', 
                    'value'=> $model->serial_id,
                    'type'=>DetailView::INPUT_SELECT2,
                        'widgetOptions'=>[
                        'data'=>ArrayHelper::map(Computer::find()->orderBy('computer_name')->asArray()->all(), 'serial_id','computer_name'),
                        'options' => ['placeholder' => 'Select...'],
                        'pluginOptions' => ['allowClear'=>true, 'width'=>'100%'],
                        ],
                ],
                [
                    'attribute'=>'damage_user_id',
                    'value'=>$model->damage_user_id,
                    'type'=>DetailView::INPUT_SELECT2,
                    'widgetOptions'=>[
                    'data' => ArrayHelper::map(Student::find()->all(),'id','username'),
                    'options' => ['placeholder' => 'Select a student..'],
                     'pluginOptions' => ['allowClear' => true],
                    ],
                ], 
            ],
        ],  
     [
        'columns' => [
            [
                'attribute'=>'damage_explain', 
                'value'=> $model->damage_explain,
                'type'=>DetailView::INPUT_TEXTAREA, 
                'options'=>['rows'=>10]
            ],
        ],
    ],          
    
        [
        'group'=>true,
        'label'=>'SECTION 2: Damaged Date and Repaired Date',
        'rowOptions'=>['class'=>'info']
        ],
        [
            'columns' => [
            [
             
                'attribute'=>'registered_at', 
                'label'=>'Registered date',
                'type'=>DetailView::INPUT_DATE,
                'widgetOptions'=>[
                'name' => 'Registered at', 
                    'options' => ['placeholder' => 'Select date ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ],
                ],
                'valueColOptions'=>['style'=>'width:30%']
             ], 
             [
             
                'attribute'=>'repaired_date', 
                'label'=>'Repaired date',
                'type'=>DetailView::INPUT_DATE,
                'widgetOptions'=>[
                'name' => 'registered_at', 
                    'options' => ['placeholder' => 'Select date ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ],
                ],
             ],             
            ],
        ],
    ];
    
    echo DetailView::widget([
    'model' => $model,
    'attributes' => $attributes,
    'condensed'=>true,
    'hover'=>true,
    'mode' => DetailView::MODE_VIEW,
    'fadeDelay'=>600,
    'panel' => [
        'heading'=>'<img src="'.Yii::$app->request->BaseUrl.'/assets/images/kommuneimage.png" '
        . 'alt="service invoice" class = "utdanningimage img-responsive">'
        .'<h1>Service lapp for IKT utstyr</h1>',
        'footer'=>'<p>Brukere av IKT-utstyr fra skolen er pliktig til å melde ifra '
        . 'om tap eller skade på PC/Mac eller annet IKT-utstyr straks dette inntreffer '
        . 'ifølge IKT-kontrakten de har signert. IKT avdelingen er'
        . ' ikke ansvarlig for personlig data på maskinen. '
        . 'Eleven/læreren er selv ansvarlig for at backup er gjort innen innlevering. '
        . 'Signerer du gir du tillatelse til at personlig data som ligger på '
        . 'maskinen ikke er IKT avdelingens ansvar og at data kan bli tapt som '
        . 'følge av at maskinen er på service.</p>'
        .'<table><tr><td><p>Signatur student : _____________</p>
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td colspan="2">
<p>IKT ansvarlig : _____________</p>
</td>
</tr>
<tr>
<td><strong>Utdanningsetaten</strong></td><td></td>
<td>
<p><span><strong>Besøksadresse:</strong></span></p>
</td>
<td></td>
</tr>
<tr>
<td>Bjørnholt skole<p> Ungdomstrinnet: Slimeveien 15</p></td>
<td></td>
<td><p>Telefon: 23463500</p>
<p>Videreg&aring;ende trinnet: Slimeveien 17 </p><p>Fax:23563610</p></td>
<td></td>
</tr>
<tr>
<td>
<p><strong>Postadresse:</strong></p>
<p>Slimeveien 15-17</p>
<p>1277 Oslo</p>
</td><td></td><td></td>
<td></td>
</tr>
<tr>
<td colspan="3">E-mail: 
<a href="mailto:Bjornholt@ude.oslo.kommune.no">
<span>Bjornholt@ude.oslo.kommune.no</span></a> 
<a href="http://www.bjornholt.sk.oslo.no">
<span>www.bjornholt.sk.oslo.no</span></a></td><td></td>
</tr>
</table>',
        'type'=>DetailView::TYPE_DEFAULT,
        ],
//     'container' => ['id'=>'kv-demo'],
     'buttons1'=> '{update}',
//   download  'formOptions' => ['action' => Url::current(['#' => 'kv-demo'])] // your action to delete
    ]) ?>

</div>
