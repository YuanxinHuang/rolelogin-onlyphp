<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php echo '<label> My old email: '.$model->email.'</label>'; ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true])
            ->hint('please enter your new email')?>
    <!--t('Vi vil ikke dele din private epostadresse med andre')?>-->
    <?php
        if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id)['teacher'])){
        $form->field($model, 'class_nr')->textInput(); 
        
        }?>
   <div class="form-group">
      <?= Html::submitButton('submit', ['class' => 'btn btn-success']) ?>
   </div>
    <?php ActiveForm::end(); ?>

</div>
