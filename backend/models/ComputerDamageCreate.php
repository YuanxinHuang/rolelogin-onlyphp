<?php
namespace backend\models;

use backend\models\ComputerDamage;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use Yii;

/**
 * Signup form
 * Form model to enforce validation rules or other methods
 * data comes in from a controller
 */
class ComputerDamageCreate extends Model
{
    public $serial_id;
    public $damage_user_id;
    public $damage_explain;
    public $registered_at;
    public $image;
  
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['serial_id', 'required'],
            ['serial_id', 'unique', 'targetClass' => '\common\models\Computer', 'message' => 'This serial number has already been taken.'],
            ['serial_id', 'string', 'max' => 25],        
            ['damage_user_id', 'required'],        
            ['damage_user_id', 'integer'],
            ['damage_user_id', 'unique', 'targetClass' => '\common\models\Student', 'message' => 'This user has already a damage.'],
            ['damage_explain', 'required'],
            ['damage_explain', 'string'],
            ['registered_at', 'required'],
            ['registered_at',  'safe'],
    
        ];
    }


     public function signup()
    {
         if ($this->validate()) {
            $computer_damage = new ComputerDamage();
            $computer_damage->serial_id = $this->serial_id;
            $computer_damage->damage_user_id = $this->damage_user_id;
            $computer_damage->damage_explain = $this->damage_explain;
            $computer_damage->registered_at = $this->registered_at;
            $computer_damage->repaired_date = null;
  
            if ($computer_damage->save()) {
                return $computer_damage;
            } else {
                return null;
            }
        } else {
            return false;
        }
    } 
    
      public static function getClassList()
    {
    $droptions = Class_computer::find()->asArray()->all();
    return Arrayhelper::map($droptions, 'class_id', 'class_describe');
    }
    
}
