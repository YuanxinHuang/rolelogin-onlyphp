<?php
namespace backend\models;

use common\models\Computer;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use Yii;

/**
 * Signup form
 * Form model to enforce validation rules or other methods
 * data comes in from a controller
 */
class ComputerCreate extends Model
{
    public $serial_id;
    public $computer_status;
    public $type_id;
    public $model;
    public $computer_name;
    public $cpu;
    public $ram;
    public $storage_capacity;
    public $shelf_placement;
    public $purchase_date;
    public $warranty_date;
    public $end_dato;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['serial_id', 'required'],
            ['serial_id', 'unique', 'targetClass' => '\common\models\Computer', 'message' => 'This serial number has already been taken.'],
     
            ['purchase_date', 'required'],
            ['warranty_date', 'required'],
            ['end_dato', 'required'],
            ['purchase_date',  'safe'],
            ['warranty_date',  'safe'],
            ['end_dato', 'safe'],      
            ['serial_id', 'string', 'max' => 25],
            ['computer_status', 'string', 'max' => 20],
            ['model' ,'string', 'max' => 20],
            ['computer_name', 'string', 'max' => 20],
            ['shelf_placement', 'string', 'max' => 20],
        ];
    }


     public function signup()
    {
         if ($this->validate()) {
            $laptop = new Computer();
            $laptop->serial_id = $this->serial_id;
            $laptop->computer_status = 10;
            $laptop->model = $this->model;
            $laptop->computer_name = $this->computer_name;
            $laptop->shelf_placement = $this->shelf_placement;
            $laptop->purchase_date = $this->purchase_date;
            $laptop->warranty_date = $this->warranty_date;
            $laptop->end_dato = $this->end_dato;

            if ($laptop->save()) {
                return $laptop;
            } else {
                return null;
            }
        } else {
            return false;
        }
    } 
    
      public static function getClassList()
    {
    $droptions = Class_computer::find()->asArray()->all();
    return Arrayhelper::map($droptions, 'class_id', 'class_describe');
    }
    
}
