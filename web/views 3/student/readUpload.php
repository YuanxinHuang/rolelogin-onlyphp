<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;
use yii\grid\GridView;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $model common\models\StudentUpload */

$this->title = Yii::t('app','Add Students and empolyee');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Students and employee'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index wrapper">

 <?php $form = ActiveForm::begin(); ?>
 <?php   
  $model->displayData();
 ?>
   <div class="form-group">
       <span class="glyphicon glyphicon-upload"></span>
 <?= Html::submitInput(Yii::t('app','Yes, want to insert to database'), ['class' => 'btn btn-default file-loading']) ?>
   </div>

    <?php ActiveForm::end(); ?>
</div>
