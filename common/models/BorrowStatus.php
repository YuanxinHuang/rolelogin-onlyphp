<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "borrow_status".
 *
 * @property integer $id
 * @property string $borrow_status_name
 * @property integer $borrow_status_value
 * @property string $description
 */
class BorrowStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'borrow_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['borrow_status_name', 'borrow_status_value', 'description'], 'required'],
            [['borrow_status_value'], 'integer'],
            [['borrow_status_name'], 'string', 'max' => 30],
            [['description'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'borrow_status_name' => Yii::t('app', 'Borrow Status Name'),
            'borrow_status_value' => Yii::t('app', 'Borrow Status Value'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
