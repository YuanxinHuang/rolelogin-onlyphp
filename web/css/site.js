/* 
 * Copyright(C) 2016.  All rights reserved to Bjørnholt school. 
 * @version 1.02.01
 */

/* 
 * function for change arrow direction 
 */
$(function() {
	$("#bt").click(function() {
		$("#bt").toggleClass("bt-change");
	});
});

$(function() {
	$("#languageItem").hover(function() {
		$("#languageItem").toggleClass("bt-change");
	});
});
/* 
 * function for tooltip 
 */
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

/* 
 * function for check student in checkout list 
 */
$('#button_student_ikt').one('click',function() {
        var keys = $('#grid').yiiGridView('getSelectedRows');
        console.log(keys);
        $.post({
           url: 'student/checkout', // your controller action
           dataType: 'json',
           data: {keylist: keys},
           success: function(data) {
                if (data.status === 'success') {
              alert('I did it! Processed checked rows.')
                }
           },
        });
    });
    /* 
 * function get different title sticky from dataTable
 */

$(document).ready(function(){
    $(".sticky-header").floatThead({scrollingTop:50});
});

//get the create event of the create button
//$('#modal_student_create_button').click(function(){
//    $('#modalAddstudent').modal('show')
//
$(function(){
      $(document).on('click', '#modal_student_create_button', function(){
            $('#modalAddstudent').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('value'));
//dynamiclly set the header for the modal via title tag
            document.getElementById('modalHeader').innerHTML = '<h1>' + $(this).attr('title') + '</h1>';
    });
});

//get different chosen student from dataTable
$(document).ready(function(){
    $(".sticky-header").floatThead({scrollingTop:50});
});