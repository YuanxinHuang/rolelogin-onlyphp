<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $model common\models\Student */

//$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'My personal information', 'url' => ['mypage']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view wrapper">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Change my Email addrss', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
<?php
Modal::begin([
    'header' => '<h4 class="modal-title">Detail View of my profile</h4>',
//    'toggleButton' => ['label' => '<i class="glyphicon glyphicon-th-list"></i> Detail View in Modal', 'class' => 'btn btn-primary']
]);
echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'email',
            'role.role_name',
              [
                'label' => 'I started school at:',
                'attribute'=>'created_at',
                'value'=>$model->created_at,
                'format' =>'datetime',
             ],
            [
                'label' => 'I ended/will end school at:',
                'attribute'=>'ended_at',
                'value'=>$model->ended_at,
                'format' =>'datetime',
             ],
        ],
        
       
    ]); 
Modal::end();?>


</div>
