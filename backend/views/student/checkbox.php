<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = ['label' => 'Borrow equipments', 'url' => ['checkout']];

?>
<div class="student-checkbox wrapper">
<?=Html::beginForm(
    ['student/checkout'],'post'
        );?>
    <h1>Choose users who need to borrow computer/equipment</h1>
    <h1>Step 1</h1>

     <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
             [
                'label' => 'Natoinal number',
                'attribute'=>'username',
                'value'=>'username',
             ],
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
             'email',
            // 'user_type_id',
            // 'status_id',
             'created_at',
             'ended_at',
            'class_nr',
            ['class' => 'yii\grid\CheckboxColumn'],
        ],
     'options' => [
            'id' => 'grid',
        ],
    ]); ?>
<?=Html::submitButton('Send requests to Computer Department', [
    'class' => 'btn btn-primary',
    ]);?>
<?= Html::endForm();?> 
</div>
