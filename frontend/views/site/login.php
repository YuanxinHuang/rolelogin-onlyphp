<!--Users login-->
<?php
    /* @var $this yii\web\View */
    /* @var $form yii\bootstrap\ActiveForm */
    /* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\icons\Icon;
Icon::map($this,Icon::FA);

$this->title = 'Bruker Logg inn';
    ?>
    <div id="content">
        <div class="desktop-only">
        <svg class="mh6 w vat font-effect-shadow-multiple" viewBox="0 0 315 28">
           <text class="w lsxs  ttu" x="35" y="20"><tspan class="fillMain">DATA REGISTER&#8208</tspan>
               <tspan class="fillDescript">BJØRNHOLT SKOLE</tspan>
           </text>
         </svg>
      </div>

    
<div class="row">  
    <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 login-wrapper box-shadow login_box">
        
    <div>
            <div class="image-logo hidden-sm hidden-xs">
                <span class="icon-stack">
              <?php 
             echo Icon::show('user', ['class'=>'fa-4x'], Icon::FA);
              ?>
                  </span>
            </div>
    </div>
        
    <div class="site-login">
        <div class="info"><h2><span class="icon-stack hidden-md hidden-lg hidden-xl">
          <?php 
         echo Icon::show('user', ['class'=>'fa-sm'], Icon::FA);
//       echo Icon::showStack('circle', 'user', ['class'=>'fa-lg'], ['class'=>'fa-inverse'],Icon::FA);
  
          ?>
          </span><?= Html::encode($this->title) ?></h2>
          <p>Skriv inn følgende for å logge inn:</p></div>
        
       
        <div>
            <div class="col-md-12">
                
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                
                <?=
                    $form->field($model, 'username')
                    ->label('Fødselsnummer')
                ?>
                <?=
                    $form->field($model, 'password')
                    ->label('Passord')
                    ->passwordInput()
                ?>
                <!--                <div style="color:#999;margin:1em 0">
                                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                                </div>-->
<!--                <div class="pull-left col-xs-12 col-md-4">
                <a href="request_password_reset">Glemt passord?</a>
                </div>-->
                
                <div class="pull-right language bg-success col-xs-12 col-md-4">
<?= Html::submitButton('Logg inn', ['class' => 'btn col-xs-12 bg-success', 'name' => 'login-button']) ?>
                
                </div>
<?php echo '<a href="'.Yii::$app->request->baseUrl.'/index.php/site/request_password_reset">Glemt passord?</a>';?>

<?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    </div>
</div>
</div>