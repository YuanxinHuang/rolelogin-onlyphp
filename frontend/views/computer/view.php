<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Computer */

$this->title = $model->serial_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Computers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="computer-view wrapper">
 <p>
        <?= Html::a(Yii::t('app', 'Back to list '), ['index'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'serial_id',
            'computer_status',
//            'type_id',
            'model',
//            'computer_name',
//            'cpu',
//            'ram',
//            'storage_capacity',
            'shelf_placement',
            'purchase_date',
            'warranty_date',
            'end_dato',
        ],
    ]) ?>

</div>
