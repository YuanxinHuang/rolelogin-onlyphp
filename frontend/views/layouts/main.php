<?php
/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use kartik\icons\Icon;
use common\models\RecordHelpers;

Icon::map($this,Icon::FI);

    frontend\assets\AppAsset::register($this);
    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
          <div class="all-container icon">
        <?php
    NavBar::begin([
        'brandLabel' =>'',
        'brandUrl' => Yii::$app->homeUrl,
        'brandOptions' => ['class' => 'brandLogo'],//options of the brand
        'options' => [
            'class' => 'navbar-default navbar-fixed-top headerbar',
        ],
    ]);
            $menuItems = [
               ['label' => Icon::show('no', [], Icon::FI).'Norsk',
            'options' => ['class' => 'language'],
            'url' => '#'],    
               ['label' => Icon::show('gb', [], Icon::FI).'English', 
            'options' => ['class' => 'language'],
            'url' => '#',
           ]
    ];
            if (Yii::$app->user->isGuest) {
            } else {
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'options' => ['class' => 'language'],
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
                 $menuItems[] = [
                    'label' => 'Help',
                    'options' => ['class' => 'language'],
                    'url' => ['/site/help'],
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
                 'encodeLabels' => false,
            ]);
            NavBar::end();
            ?>
            <div class="row-fluid-aside">
 <?php
    if (!Yii::$app->user->isGuest){
      echo '<!--fluid-aside div start-->
          <aside class="main-sidebar bg-light-orange">
                    <!--sidebar-menu div start-->
                    <div id="sidebar-menu">
                        <!-- sidebar ul start-->
                        <ul class="nav-list nav" id="sidebar-menu-id">';
    }?>    
            <?php  
            if (Yii::$app->user->can('send_student_list')){
                       echo '<li>
                                <label class="tree-toggle nav-header">Utstyr</label>
                                <ul class="nav nav-list tree">
                                    <li><a href="'.Yii::$app->request->baseUrl.'/index.php/computer/index">Datamaskin</a></li>
                                </ul>
                            </li>
              
                       <li><label class="tree-toggle nav-header">Utlån</label>
                                <ul class="nav nav-list tree">
                                    <li><a href="'.Yii::$app->request->baseUrl.'/index.php/student/checkout"><i class="glyphicon glyphicon-user"></i>Ny utlån</a></li>
                                    <li><a href="'.Yii::$app->request->baseUrl.'/index.php/student_equipment/teachers_request"><i class="glyphicon glyphicon-th-list"></i>Oversikt</a></li> 
                                </ul>
                            </li>';
                            }?>
 
            <?php
            if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id)['student'])){
                if (false!==RecordHelpers::userHas('user_equipment')){
                        echo '<li><label class="tree-toggle nav-header">My rent</label>
                                <ul class="nav nav-list tree">
                                    <li><a href="'.Yii::$app->request->baseUrl.'/index.php/student_equipment/my_rent"><i class="glyphicon glyphicon-user"></i>My rent</a></li>
                                </ul>
                </li>';}
            }?>
               
               
            <?php
            if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id)['teacher'])){
                   if (false!==RecordHelpers::userHas('user_equipment')){       
                echo '<li><label class="tree-toggle nav-header">My rent</label>
                                <ul class="nav nav-list tree">
                                    <li><a href="'.Yii::$app->request->baseUrl.'/index.php/student_equipment/my_rent"><i class="glyphicon glyphicon-user"></i>My page</a></li>
                                </ul>
                            </li>';
                }
            }?>
            <?php

            if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id)['accounting_department'])){
                if (false!==RecordHelpers::userHas('user_equipment')){ 
                                          echo '<li><label class="tree-toggle nav-header">My rent</label>
                                <ul class="nav nav-list tree">
                                    <li><a href="'.Yii::$app->request->baseUrl.'/index.php/student/index"><i class="glyphicon glyphicon-user"></i>My rent</a></li>
                                </ul>
                            </li>';}
                            
                          echo '<li><label class="tree-toggle nav-header">Student status</label>
                                <ul class="nav nav-list tree">
                                    <li><a href="'.Yii::$app->request->baseUrl.'/index.php/student/index"><i class="glyphicon glyphicon-user"></i>Student status</a></li>
                                </ul>
                            </li>';
                
            }?>
            <?php
            if (!Yii::$app->user->isGuest){  
              echo  '<li>
                                <label class="tree-toggle nav-header"><span class="glyphicon glyphicon-user"></span>Min side</label>
                                <ul class="nav nav-list tree">
                                    <li><a href="'.Yii::$app->request->baseUrl.'/index.php/student/mypage">Setting Epost</a></li> 
                                         <li><a href="'.Yii::$app->request->baseUrl.'/index.php/student/changepassword">Endre passordet</a></li> 
                                </ul>
                            </li>
                            <!-- sidebar ul end-->
                        </ul>
                        <!--sidebar-menu div end-->
                    </div>
            </aside>
                    <!--sider aside end-->
                ';}?>
                <div id = "main_content_admin">
                    <div id = "main_container_admin">
                     <?=
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                    ?>
                <?= Alert::widget() ?>
                <?= $content ?>
                    </div>
                </div>
                </div>
        </div>   

            <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
