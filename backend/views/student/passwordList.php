<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use common\models\Student;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

    $this->title = Yii::t('app','Update password');
    $this->params['breadcrumbs'][] = $this->title;

?>
  <?php
    Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id'=>'modalAddstudent',
    'size'=> 'modal-md',
    ]);
//    $newmodelCreate = new backend\models\StudentCreate();
    echo "  <div id='modalContent'>
                <div class='load'>
                </div>
            </div>";
    Modal::end();?>
    
    <div class="inline wrapper">
        <h1><i class="fa fa-flag-o-red"></i><strong><?= $this->title?></strong></h1>
      
        <?= GridView::widget([
             'options' => [
                    'id' => 'studentGrid'
                ],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
             'pjax'=>true,
               'pjaxSettings'=>[
                'neverTimeout'=>true,
            ],
            'rowOptions'=>function($model){
                        if($model->status_id == '99'){
                            return ['class' =>'danger'];
                        } elseif($model->status_id == '10') {
                            return ['class' =>'success'];
                        } else {
                            return ['class' =>'default'];
                        }
            },
            'columns' => [
//                'id',
                [
                 'attribute'=>'username',
                 'label' => Yii::t('app', 'Personal number'),
                 'value'=>'username',
                 ],
                [
                 'attribute'=>'email',
                 'label' => Yii::t('app', 'email'),
                 'value'=>'email',
                 ],
                [
                 'attribute'=>'role_id',
                 'label' => Yii::t('app', 'Role'),
                 'value'=>'role.role_name',
                 'filter'=> ArrayHelper::map(\backend\models\Role::find()->all(),'role_value','role_name'),
                 ],
                [
                 'attribute'=>'status_id',
                 'label' => Yii::t('app', 'Status is'),
                 'value'=>'status.status_name',
                 'filter'=>array("10"=>  Yii::t('app', "ACTIVE"),"20"=>Yii::t('app', "FEE NOT PAID"),"30"=>Yii::t('app', "DAMAGED MACHINE"),"99"=>Yii::t('app', "DEACTIVATE")),
                 ],
                [
                'label' => 'This person is:',
                'attribute'=>'user_type_id',
                'value'=>'studentTypeName',
                'filter'=>array("10"=>  Yii::t('app', "Primary High School"),"20"=>Yii::t('app', "High School")),
                ],
                 ['class' => 'yii\grid\ActionColumn',
                  'header'=> Yii::t('app', 'Change password'),
                  'headerOptions' => ['width' => '60'],
                          'template'=>'{setnewpassword}',
                            'buttons'=>[                            
                                          'setnewpassword' => function ($url, $model) {     
                                  return Html::a('<span class=" btn btn-success">'.Yii::t('app','Change').'</span>', $url, [
                                        'title' => Yii::t('yii', 'setnewpassword'),
                                ]);                            
                              },
                               
                          ]              
             ],
            ],
        'panel' => [
       
        'before'=>Html::a('<i class="glyphicon glyphicon-repeat"></i>'. Yii::t('app', 'Reset Search Fields') , ['index'], ['class' => 'btn btn-info']),
        'footer'=>false
    ],
        'export' => false,
        ]); ?>
   
    </div>
</div>
