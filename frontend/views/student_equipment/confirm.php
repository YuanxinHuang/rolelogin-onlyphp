<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Student Equipments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-equipment-index wrapper">

    <h1><?= Html::encode($this->title) ?></h1> 
<?php 
//echo Html::beginForm();
?> <?php 
//echo Html::a('Create Student Equipment', ['create'], ['class' => 'btn btn-success']) 
   ?>
<?php
//echo Html::submitButton('Confirm Requests and Send to IKT department', [
//    'class' => 'btn btn-primary',
//    ]);
    ?>
<?php
//echo Html::endForm();
?> 

   
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'student.email',
            'student.username',
            'status.borrow_status_name',
            'Borrowed by'=>'teacher.username',
            'Start at'=>'start_at',
            'End at'=>'end_at',
        ],
         'options' => [
            'class' => 'col-sm-12',
        ],
    ]); ?>

</div>
</div>
