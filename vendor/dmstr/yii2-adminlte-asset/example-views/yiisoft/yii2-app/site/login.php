<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */


use kartik\widgets\ActiveForm;
use kartik\icons\Icon;
use kartik\widgets\ActiveField;

Icon::map($this, Icon::FA);

$this->title = 'ADMIN SIGN IN';
//$this->params['breadcrumbs'][] = $this->title;
  ?>
<!--Admin login-->


<div>
    <div class="login-box">
        <div class="login-logo desktop-only">
            <a href="#"><b>DATA-</b>REGISTER</a>
            <p href="#"><b>BJØRNHOLT SKOLE</b></p>
        </div>
    
    </div>
    <div class="row login_image">  
        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 box-shadow login-wrapper">
            <div class="image-logo hidden-sm hidden-xs">
                <span class="icon-stack">
                    <?php
                    echo Icon::show('user', ['class' => 'fa-4x icon-stack-1x'], Icon::FA);
                    echo Icon::show('wrench', ['class' => 'fa-2x icon-stack-2x'], Icon::FA);
                    ?>
                </span>
            </div>
            <div class="site-login fadeInUp animated">
                <div class="info"><h2><span class="icon-stack hidden-md hidden-lg hidden-xl">
                            <?php
                            echo Icon::show('wrench', ['class' => 'fa-sm'], Icon::FA);
                            ?>
                        </span><?= Html::encode($this->title) ?></h2><p>
                    <?php echo Yii::t('app', 'Please fill this form for login:') . '</p><p>Test med brukernavn-admin og passord-asdf1234</p>'; ?>
                </div>
                <div class="col-md-12">
                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                    <?=
                            $form->field($model, 'username', [
                        'hintType' => ActiveField::HINT_SPECIAL,
                        'hintSettings' => ['placement' => 'top', 'onLabelClick' => true,'onLabelHover' => false]
                    ])      ->hint('Norwegian Person Number is National identification number consists of 11 digits in the form DDMMYY-#####')
                            ->label(Yii::t('app', 'Norwegian Person Number'))->textInput(array('placeholder' => Yii::t('app', 'Norwegian Person Number(11 numbers)')))
                    ?>
                    <?=
                            $form->field($model, 'password', [
                        'hintType' => ActiveField::HINT_SPECIAL,
                        'hintSettings' => ['placement' => 'top','onLabelClick' => true,'onLabelHover' => false]
                    ])      ->hint('Resetting password emails are sent by ICT department at the beginning of each semester. If you didn´t receive any password resetting email, you can click <a href="' . Yii::$app->request->baseUrl . '/admin.php/site/request_password_reset">' .
                    Yii::t('app', 'Forget your password?').'</a> to resend one. Use the email registered in SATS system for resetting password. <p>Please contact us if you have any questions.</p>')
                            ->label(Yii::t('app', 'Password'))->passwordInput(array('placeholder' => Yii::t('app', 'Password(At least 6 letters, max 12 letters)')))
                    ?>
                    <div class="pull-right language bg-success col-xs-12 col-md-4">
                    <?= Html::submitButton(Yii::t('app', 'Log in'), ['class' => 'btn col-xs-12 bg-success',
                        'name' => 'login-button'])
                    ?>
                    </div> 
                    <?php echo '<a href="' . Yii::$app->request->baseUrl . '/admin.php/site/request_password_reset">' .
                    Yii::t('app', 'Forget your password?').'</a>';
                    ?>

<?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

