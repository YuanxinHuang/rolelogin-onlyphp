<?php
namespace common\models;

use common\models\Student_equipment;
use yii\base\Model;
use Yii;

/**
 * Signup form
 * Form model to enforce validation rules or other methods
 * data comes in from a controller
 */
class Student_equipment_create extends Model
{
    public $renter_id;
    public $serial_id;
    public $borrow_status_id;
    public $require_by_teacher_id;
    public $start_at;
    public $end_at;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['renter_id', 'filter', 'filter' => 'trim'],
            ['renter_id', 'required'],
            ['renter_id', 'unique', 'targetClass' => '\common\models\Student', 'message' => 'This username has already been taken.'],
       

//            ['start_at', 'filter', 'filter' => 'trim'],
            ['start_at', 'required'],
//
//            ['end_at', 'filter', 'filter' => 'trim'],
            ['end_at', 'required'],
            
            
        ];
    }

    /**
     * Signs student_equiment up. Here the equipment means computer
     *
     * @return User|null the saved model or null if saving fails
     */

    
     public function teacher_request_rent($key)
    {
        
//        This helpe Student uoload to validate data
//        if ($this->validate()) {
            $student_equipment = new Student_equipment();
//                $this->renter_id = $newstudent->id;
//                $this->borrow_status_id = 10;
//                $this->require_by_teacher_id = Yii::$app->user->id;
//                $this->start_at=time();
//                $this->end_at=time();
             $newstudent = Student::findOne($key);
            $student_equipment->renter_id = $newstudent->id;
            $student_equipment->borrow_status_id = 10;
            $student_equipment->require_by_teacher_id = Yii::$app->user->id;
//            $student_equipment->$serial_id = null;
//            $student_equipment->$start_at = 1234;
//            $student_equipment->$end_at = 21234;
//            $user->user_type_id = 10;
//            $user->role_id = $this->role_id;
//            $user->created_at = 1;
//            $user->updated_at = 1;
//            $user->class_nr = $this->class_nr;
 
        if ($student_equipment->save()) {
//        $auth = Yii::$app->authManager;
//        $authorRole = $auth->getRole($user->getRoleName());
//        $auth->assign($authorRole, $user->getId());
            return $student_equipment;
        } else {
            return null;
        }
    }

    }
    
        

