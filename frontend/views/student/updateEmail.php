<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Student */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Update System user: ' . ' ' . $model->username;
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-update wrapper">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(); ?>
    <?php echo '<label> My old email: '.$model->email.'</label>'; ?>
    <?= $form->field($model, 'email',[
    'addon' => ['prepend' => ['content'=>'New email','options'=>['class'=>'alert-success']]]
])->textInput(['maxlength' => true])
    ?>
    <!--t('Vi vil ikke dele din private epostadresse med andre')?>-->
    <?php
        if (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id)['teacher'])){
        echo $form->field($model, 'class_nr',[
        'feedbackIcon' => [
        'prefix' => 'fa fa-',
        'default' => 'phone',
        'success' => 'check-circle',
        'error' => 'exclamation-circle',
    ],
    'addon' => [
        'prepend' => [
            'content'=>'New Class name','options'=>[
                'class'=>'alert-success']]]
])->widget('yii\widgets\MaskedInput', [
    'mask' => 'AAA-99-A'
]);
        
        }?>
   <div class="form-group">
      <?= Html::submitButton('submit', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', ['mypage'], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to leave this page without saving?',
                'method' => 'post',
            ],
        ]) ?>
   </div>
    <?php ActiveForm::end(); ?>

</div>