<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "student_type".
 *
 * @property integer $id
 * @property string $student_type_name
 * @property integer $student_type_value
 */
class StudentType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_type_name', 'student_type_value'], 'required'],
            [['student_type_value'], 'integer'],
            [['student_type_name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student_type_name' => Yii::t('app', 'Student Type Name'),
            'student_type_value' => Yii::t('app', 'Student Type Value'),
        ];
    }
}
