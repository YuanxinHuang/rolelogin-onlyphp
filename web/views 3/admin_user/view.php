<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Admin2 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'My information side'), 'url' => ['mypage']];
?>
<div class="admin-view wrapper">
    <h1><i class="fa fa-info-circle" aria-hidden="true"></i><strong>Information</strong></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'email:email',
            'role_id',
    

        ],
    ]) ?>

</div>
