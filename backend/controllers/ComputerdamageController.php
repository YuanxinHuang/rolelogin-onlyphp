<?php

namespace backend\controllers;

use Yii;
use backend\models\ComputerDamage;
use backend\models\ComputerDamageSearch;
use backend\models\UploadForm;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\base\InvalidCallException;
use kartik\mpdf\Pdf;

/**
 * ComputerDamageController implements the CRUD actions for ComputerDamage model.
 */
class ComputerdamageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ComputerDamage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ComputerDamageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ComputerDamage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    $model=$this->findModel($id);
    $post = Yii::$app->request->post();   
    // process ajax delete
    if (Yii::$app->request->isAjax && isset($post['kvdelete'])) {
        echo Json::encode([
            'success' => true,
            'messages' => [
                'kv-detail-info' => 'The record was successfully deleted. ' . 
                    Html::a('<i class="glyphicon glyphicon-hand-right"></i>  Click here', 
                        ['/computerdamage/view'], ['class' => 'btn btn-sm btn-info']) . ' to proceed.'
            ]
        ]);
        return;
    }
    // return messages on update of record
        $modelfile = new UploadForm();
        $modelfile->file = UploadedFile::getInstance($model, 'image');
        if ($model->load(Yii::$app->request->post())) {
            $modelfile->uploadimage();
            $model->image = $modelfile->file->name;
            if ($model->save()){
        Yii::$app->session->setFlash('kv-detail-success', 'Success updated');
        return $this->redirect(['view', 'id'=>$model->id]);
          } else{
            return $this->render('view', [
                'model' => $model]);
            }
    }
    return $this->render('view', ['model'=>$model]);
    }

    /**
     * Creates a new ComputerDamage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ComputerDamage();
        $modelfile = new UploadForm();
        $modelfile->file = UploadedFile::getInstance($model, 'image');
        if ($model->load(Yii::$app->request->post()) ) {
           
            if ($modelfile->file) {
                $modelfile->uploadimage();
                $model->image = $modelfile->getName();
            } else {
                $model->image = '00.png';
            }
            if ($model->save()){
                Yii::$app->session->setFlash('success', 'You had added a new record.');
                return $this->redirect(['view', 'id' => $model->id]);
            } else{
            return $this->render('create', [
                'model' => $model]);
            }
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing ComputerDamage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelfile = new UploadForm();
        if (Yii::$app->request->isPost &&$model->load(Yii::$app->request->post())) {
//         $inFile = $modelfile->file = UploadedFile::getInstance($model, 'image');
//         $size = 2 * 1024 * 1000;
//            if (!$inFile->size || $inFile->size > $size) {
//                \Yii::$app->getSession()->setFlash('error', 'File canot be bigger than 2Mb.');
//                return $this->render('update', ['model' => $model]);
//            }
//            $modelfile->uploadimage();
//            $model->image = $modelfile->file->name;
            if ($model->save()){
        Yii::$app->session->setFlash('kv-detail-success', 'Success updated');
        return $this->redirect(['view', 'id'=>$model->id]);
          } else{
             Yii::$app->session->setFlash('kv-detail-success', 'You had given wrong info.');
            return $this->render('update', [
                'model' => $model]);
            } 
        }else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * Download an pdf by link to another page 
     * If report is successful, the browser will be redirected to the another page.
     * @param integer $id
     * @return pdf
     */
    
    public function actionDownloadpdf($id){
         $model = $this->findModel($id);
         $pdf = Yii::$app->pdf;
         $pdf->mode = Pdf::MODE_CORE;
         $pdf->cssFile = Yii::$app->request->BaseUrl.'/css/reportPdf.css';
         $pdf->options = ['title' => 'Damage service Invoice'];
         $pdf->methods = [ 
            'SetHeader'=>['Damage Report'], 
            'SetFooter'=>['{PAGENO}'],
        ];
         $pdf->content = $this->renderPartial('view', [
                'model' => $model
            ]);
    return $pdf->render();
    }
    
     /**
     * Deletes an existing ComputerDamage model, process ajax delete
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDamagepdf_list(){
        $searchModel = new ComputerDamageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('damagepdf_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
     /**
     * Deletes an existing ComputerDamage model, process ajax delete
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
        public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ComputerDamage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ComputerDamage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ComputerDamage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
