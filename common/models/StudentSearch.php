<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Student;

/**
 * StudentSearch represents the model behind the search form about `common\models\Student`.
 */
class StudentSearch extends Student
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'role_id', 'user_type_id', 'status_id', 'created_at', 'ended_at'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'class_nr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Student::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'key' => 'id'
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'user_type_id' => $this->user_type_id,
            'status_id' => $this->status_id,
            'created_at' => $this->created_at,
            'ended_at' => $this->ended_at,
       
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'class_nr', $this->class_nr])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }


    public function getAllData()
    {
        $sql = "SELECT id, username, email, created_at, ended_at, class_nr, role_id FROM {$this->tableName()}";
        $command = $this->getDb()->createCommand($sql);
        $data =  $command->queryAll();
        return $data;
    }

    public function getAllDataByRoleType()
    {
        $sqlStudent = "SELECT system_user.id, username, email, created_at, ended_at, class_nr, role_id
                       FROM {$this->tableName()}
                       LEFT JOIN user_equipment ON system_user.id = user_equipment.renter_id
                       WHERE role_id=10 AND user_equipment.renter_id IS NULL";
        $command = $this->getDb()->createCommand($sqlStudent);
        $student = $command->queryAll();
        $sqlTeacher = "SELECT system_user.id, username, email, created_at, ended_at, class_nr, role_id
                       FROM {$this->tableName()}
                       LEFT JOIN user_equipment ON system_user.id = user_equipment.renter_id
                       WHERE role_id=20 AND user_equipment.renter_id IS NULL";
        $command = $this->getDb()->createCommand($sqlTeacher);
        $teacher = $command->queryAll();
        $sqlAccountingDepartment = "SELECT system_user.id, username, email, created_at, ended_at, class_nr, role_id
                       FROM {$this->tableName()}
                       LEFT JOIN user_equipment ON system_user.id = user_equipment.renter_id
                       WHERE role_id=30 AND user_equipment.renter_id IS NULL";
        $command = $this->getDb()->createCommand($sqlAccountingDepartment);
        $accounting_department = $command->queryAll();
        $data = array("student"=>$student, "teacher"=>$teacher, "accounting_department"=>$accounting_department);
        return $data;
    }

    public function getStudentsByIds($ids)
    {

        foreach($ids as $id)
        {
            $sql = "SELECT id, username, email, created_at, ended_at, class_nr, role_id
                       FROM {$this->tableName()}
                       WHERE id = :userid";
            $command = $this->db->createCommand($sql);
            $command->bindValue(':userid',$id);
             $row= $command->queryAll();
            $data[] =$row[0];
        }
        return $data;
    }
}
