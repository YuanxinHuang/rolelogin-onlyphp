<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\Student */

$this->params['breadcrumbs'][] = 'Update password';
?>
<div class="wrapper">

    <h1><?= Html::encode($this->title) ?></h1>

  <div>
    <?php $form = ActiveForm::begin(); ?>
 
   <?= $form->field($model, 'password_old')->passwordInput() ?>
     
   <?= $form->field($model, 'password_new')->passwordInput() ?>
   <div class="form-group">
      <?= Html::submitButton('submit', ['class' => 'btn btn-success']) ?>
   </div>
    <?php ActiveForm::end(); ?>

</div>

</div>
