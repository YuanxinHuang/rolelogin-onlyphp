<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Laptopmodel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="laptopmodel-form">
 <?php error_reporting(E_ALL); 
    $form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['deviceSize' => ActiveForm::SIZE_SMALL],
]); ?>

    <?= $form->field($model, 'model_name',[
    
    'hintType' => ActiveField::HINT_SPECIAL,
    'hintSettings' => ['placement' => 'left', 'onLabelClick' => true, 'onLabelHover' => true]
])->hint('Model name needs to be unique.')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cpu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ram')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'storage_capacity',[
    'hintType' => ActiveField::HINT_SPECIAL,
    'hintSettings' => ['placement' => 'top', 'onLabelClick' => true, 'onLabelHover' => true]
])->hint('Please fill in the Hard Disk Drives capacity.')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'computer_type_id',[
    'hintType' => ActiveField::HINT_SPECIAL,
    'hintSettings' => ['placement' => 'top', 'onLabelClick' => true, 'onLabelHover' => true]
])->hint('Computer type id needs to either Mac or PC.')->widget(Select2::classname(), [
    'data' => $model->getLaptopTypeList(),
    'options' => ['placeholder' => 'Select a Type ...'],
    ])->label() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php if($model->isNewRecord){
        echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']);
        }
        else {}
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
