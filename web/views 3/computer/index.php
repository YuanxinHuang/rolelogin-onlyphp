<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ComputerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Computers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="computer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Computer'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'serial_id',
            'computer_status',
             [
                'attribute'=>'model',
                'value'=> 'modelTypeLink',
                'format' => 'raw',
              ],
            'computer_name',
            'shelf_placement',
            // 'purchase_date',
            // 'warranty_date',
            // 'end_dato',

             ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{view} {update} ',
                            'buttons'=>[
                              'view' => function ($url, $model) {     
                                return Html::a('<span class=" btn-rounded btn-info glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('yii', 'view'),
                                ]);                            
                              },
                               'update' => function ($url, $model) {     
                                return Html::a('<span class=" btn-rounded btn-primary glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('yii', 'update'),
                                ]);                                
                              }
                          ]              
             ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
