<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\models\Student_equipment_create;
use common\models\Student;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Student Equipments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-equipment-index">

    <h1><?= Html::encode($this->title) ?></h1> 
<?php 
//echo Html::a('Create Student Equipment', ['create'], ['class' => 'btn btn-success']) 
   ?>
<?php
//echo Html::submitButton('Confirm Requests and Send to IKT department', [
//    'class' => 'btn btn-primary',
//    ]);
    ?>

<?php 
    foreach ($selection as $value) {
            $model = new Student_equipment_create();
            $model->teacher_request_rent($value);
            $form = ActiveForm::begin(); 
//            $student = array();  
//            $student[] = new student();
//            $student = $model->student;
//            $student->getStudentEmail();
         $student = Student::findOne($value);
    echo "<em>'$student->email' want to borrow for this period:</em>";
    echo $form->field($model, 'start_at')->textInput(); 
    echo $form->field($model, 'end_at')->textInput();
        echo '<div class="form-group">';
        echo Html::submitButton($model->isNewRecord ? 'Confirm and send to IKT department' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); 
   echo '</div>';
   ActiveForm::end();
    }
?>
    </div>
